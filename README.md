# sprite

#### 介绍
.net core 自定义表单引擎

#### 软件架构
架构设计参考：https://www.cnblogs.com/spritekuang/p/14032899.html

#### 使用说明

1.  执行创建数据库脚本
2.  修改数据库连接，运行项目
3.  将\99_document\postman文件导入到postman，测试相关接口

#### 完成功能

1.  表单定义本地缓存
2.  基础数据本地缓存
3.  租户/应用配置管理
4.  动态Sql
5.  动态查询条件
6.  动态表达式
7.  Object管理
8.  Property管理
9.  业务表管理
10. 运行时默认Sql执行


#### 其他未整理或未实现功能

1.  见架构设计：https://www.cnblogs.com/spritekuang/p/14032899.html
