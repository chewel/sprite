CREATE TABLE SpriteObjects(`Id` char(36) PRIMARY KEY NOT NULL,
	`ApplicationCode` VARCHAR(100) NOT NULL COMMENT '应用Code',
	`Name` VARCHAR(100) NOT NULL COMMENT '表名，新增后不能修改',
	`DisplayName` VARCHAR(100) NOT NULL COMMENT '对象显示名称',
	`Description` VARCHAR(1024) NULL COMMENT '描述',
	`Version` char(36) NOT NULL COMMENT '版本',
	`KeyType` int NOT NULL COMMENT '主键类型（业务系统原则都用Guid）',
	`IsTree` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为树',
	`CreateAudit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '创建审计(新增人Id，新增时间)',
	`ModifyAudit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '修改审计(修改人Id，修改时间)',
	`DeleteAudit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除审计(是否删除，删除人Id，删除时间)',
	`PropertyJsons` TEXT NULL COMMENT '属性冗余存储'
);

CREATE TABLE ObjectPropertys(
	`Id` char(36) PRIMARY KEY NOT NULL,
	`Name` VARCHAR(100) NOT NULL COMMENT '属性名称',
	`DisplayName` VARCHAR(100) NOT NULL COMMENT '属性显示名称',
	`Description` VARCHAR(1024) NULL COMMENT '描述',
	`ObjectId` char(36) NOT NULL COMMENT '对象Id',
	`FieldType` int NOT NULL COMMENT '字段类型',
	`IsNull` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否可空',
	`IsRequired` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否必填',
	`DefaultValue` VARCHAR(100) NULL COMMENT '默认值',
	`IsUnique` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否唯一',
	`Length` int NULL COMMENT '字段长度',
	`Length2` int NULL COMMENT '字段2长度',
	`Order` VARCHAR(20) NULL COMMENT '排序（显示用）'
);

CREATE TABLE ObjectMethods(
	`Id` char(36) PRIMARY KEY NOT NULL,	
	`ApplicationCode` VARCHAR(100) NOT NULL COMMENT '应用Code',
	`Name` VARCHAR(100) NOT NULL COMMENT '方法名称',
	`ObjectId` char(36) NOT NULL COMMENT '对象Id',
	`Description` VARCHAR(1024) NULL COMMENT '描述',
	`SqlBaseMethod` VARCHAR(100) NULL COMMENT 'Execute方法基础',
	`MethodType` int NOT NULL COMMENT '方法类型',
	`MethodExeType` int NOT NULL COMMENT '方法执行类型',
	`MethodExeContent` TEXT NULL COMMENT '方法执行内容，Sql语句时，执行的是Sql命令；反射时，传递程序集以方法，通过反射执行；微服务为微服务名称',
	`DestApplicationCode` VARCHAR(100) NULL COMMENT '目标应用Code(微服务方法时才赋值)',
	`ParamJsons` TEXT NULL COMMENT '参数冗余存储'
);

CREATE TABLE CommonParams(
	`Id` char(36) PRIMARY KEY NOT NULL,
	`BusinessId` char(36) NOT NULL COMMENT '方法Id',
	`BusinessCategory` VARCHAR(100) NULL COMMENT '业务类别',
	`ObjectId` char(36) NOT NULL COMMENT '对象Id',
	`ParamName` VARCHAR(100) NULL COMMENT '参数名称',
	`FieldType` int NOT NULL COMMENT '参数类型',
	`ParamType` int NOT NULL COMMENT '参数值类型',
	`Description` VARCHAR(1024) NULL COMMENT '描述',
	`IsRequired` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否必填',
	`Length` int NULL COMMENT '字段长度',
	`ValidateRegular` VARCHAR(500) NULL COMMENT '正则表达式验证',
	`ValidateType` int NULL COMMENT '验证类型',
	`ParamDirection` int NOT NULL COMMENT '输入或者输出类型',
	`TreePath` VARCHAR(100) NULL COMMENT '方法名称关联（Param1:Param2），需要包含自身'
);

CREATE TABLE `SpriteCacheMains` (
  `Id` varchar(256) PRIMARY KEY NOT NULL COMMENT '业务标识',
  `Version` varchar(50) NOT NULL COMMENT '版本号',
  `Notes` varchar(2000) NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='前端缓存版本管理';

INSERT INTO `SpriteCacheMains` VALUES('Framework_Dict_Cache','','数据字典缓存');

CREATE TABLE `Dicts` (
  `Id` char(36) PRIMARY KEY NOT NULL,
  `Name` varchar(256) NOT NULL COMMENT '字典名称',
  `Code` varchar(256) NOT NULL COMMENT '字典编码',
  `Notes` varchar(2000) NULL COMMENT '备注',
  UNIQUE KEY `UniqueCode` (`Code`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据字典';

CREATE TABLE DictItems (
  `Id` char(36) PRIMARY KEY NOT NULL,
  `Name` varchar(256) NOT NULL COMMENT '字典项名称',
  `Code` varchar(256) NOT NULL COMMENT '字典项编码',
  `Order` varchar(16) NULL COMMENT '排序',
  `Notes` varchar(2000) NULL COMMENT '备注',
  `IsActive` tinyint(1) NOT NULL COMMENT '是否激活',
  `DictCode` varchar(256) NOT NULL COMMENT '字典编码'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据字典项';





