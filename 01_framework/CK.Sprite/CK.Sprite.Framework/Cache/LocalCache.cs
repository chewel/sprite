﻿using AutoMapper;
using CK.Sprite.Cache;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace CK.Sprite.Framework
{
    public abstract class LocalCache<T> : BaseLocalCache<T>, ISingletonDependency
    {
        public IMapper Mapper => LazyGetRequiredService(ref _mapper);
        private IMapper _mapper;

        protected DefaultDbConfig DefaultDbConfig => LazyGetRequiredService(ref _defaultDbConfig).Value;
        private IOptions<DefaultDbConfig> _defaultDbConfig;
        
        protected ISerializeService SerializeService => LazyGetRequiredService(ref _serializeService);
        private ISerializeService _serializeService;

        public override List<T> GetAll(string applicationCode = default)
        {
            return null;
        }

        public override Dictionary<Guid, T> GetAllDict(string applicationCode = default)
        {
            return null;
        }

        public override abstract string CacheKey { get; }
    }
}
