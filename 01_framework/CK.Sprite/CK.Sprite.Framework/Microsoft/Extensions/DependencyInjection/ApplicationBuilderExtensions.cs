﻿using CK.Sprite.Framework;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace System
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseSpriteCore(this IApplicationBuilder app)
        {
            ServiceLocator.SetServices(app.ApplicationServices);

            foreach (var spriteModule in ApplicationManager._spriteModules)
            {
                spriteModule.OnApplicationInitialization();
            }
        }
    }
}
