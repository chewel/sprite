﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public interface IThirdService
    {
        /// <summary>
        /// 根据用户Id获取用户集合
        /// </summary>
        /// <param name="ids">用户Id</param>
        /// <returns></returns>
        Task<List<SpriteUser>> GetSpriteUsers(IEnumerable<string> ids);

        /// <summary>
        /// 获取单个用户信息
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <returns></returns>
        Task<SpriteUser> GetSpriteUser(string id);

        Task<List<SpriteUser>> GetSpriteUserByRoleIds(List<string> roleIds);
    }
}
