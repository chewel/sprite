﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public static class ServiceCollectionThirdDefault
    {
        public static void AddThirdDefault(this IServiceCollection services)
        {
            services.AddSingleton(typeof(ICurrentUser), typeof(DefaultCurrentUser));
            services.AddSingleton(typeof(ICurrentTenant), typeof(DefaultCurrentTenant));
            services.AddSingleton(typeof(IThirdService), typeof(LocalCacheThirdService));
            services.AddSingleton(typeof(ISpriteDeptCache), typeof(LocalSpriteDeptCache));
            services.AddSingleton(typeof(ISpriteRoleCache), typeof(LocalSpriteRoleCache));
            services.AddSingleton(typeof(ISpriteUserCache), typeof(LocalSpriteUserCache));
            services.AddSingleton(typeof(ISpriteUserRoleCache), typeof(LocalSpriteUserRoleCache));
        }
    }
}
