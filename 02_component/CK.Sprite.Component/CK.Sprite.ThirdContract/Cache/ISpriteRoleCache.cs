﻿using CK.Sprite.Cache;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public interface ISpriteRoleCache : ILocalCache<SpriteRole>
    {

    }

    public interface IRemoteSpriteRoleService
    {
        List<SpriteRole> GetAll();
    }
}
