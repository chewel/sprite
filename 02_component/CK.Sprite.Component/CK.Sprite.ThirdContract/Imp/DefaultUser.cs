﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    /// <summary>
    /// 当前租户信息
    /// </summary>
    public class DefaultCurrentUser : ICurrentUser
    {
        public string UserId => "adminId";

        public string UserName => "admin";
    }
}
