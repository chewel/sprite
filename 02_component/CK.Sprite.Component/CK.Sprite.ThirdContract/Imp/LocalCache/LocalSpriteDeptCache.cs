﻿using CK.Sprite.Cache;
using CK.Sprite.CrossCutting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public class LocalSpriteDeptCache : BaseLocalCache<SpriteDept>, ISpriteDeptCache
    {
        public IRemoteSpriteDeptService _remoteSpriteDeptService => LazyGetRequiredService(ref remoteSpriteDeptService);
        private IRemoteSpriteDeptService remoteSpriteDeptService;

        public override string CacheKey => ThirdConsts.SpriteDeptCacheKey;

        public override List<SpriteDept> GetAll(string applicationCode = null)
        {
            if (!LocalCacheContainer.IsEnabledLocalCache)
            {
                return _remoteSpriteDeptService.GetAll();
            }
            else
            {
                return (List<SpriteDept>)LocalCacheContainer.Get(CacheKey, key =>
                {
                    return _remoteSpriteDeptService.GetAll();
                });
            }
        }

        public override Dictionary<Guid, SpriteDept> GetAllDict(string applicationCode = null)
        {
            return null;
        }

        public async Task<SpriteDept> GetById(string id)
        {
            var result = GetAll().FirstOrDefault(r => r.Id == id);
            return await Task.FromResult(result);
        }
    }
}
