﻿using CK.Sprite.Cache;
using CK.Sprite.CrossCutting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public class LocalSpriteUserRoleCache : BaseLocalCache<SpriteUserRole>, ISpriteUserRoleCache
    {
        public IRemoteSpriteUserRoleService _remoteSpriteUserRoleService => LazyGetRequiredService(ref remoteSpriteUserRoleService);
        private IRemoteSpriteUserRoleService remoteSpriteUserRoleService;

        public override string CacheKey => ThirdConsts.SpriteUserRoleCacheKey;

        public override List<SpriteUserRole> GetAll(string applicationCode = null)
        {
            if (!LocalCacheContainer.IsEnabledLocalCache)
            {
                return _remoteSpriteUserRoleService.GetAll();
            }
            else
            {
                return (List<SpriteUserRole>)LocalCacheContainer.Get(CacheKey, key =>
                {
                    return _remoteSpriteUserRoleService.GetAll();
                });
            }
        }

        public override Dictionary<Guid, SpriteUserRole> GetAllDict(string applicationCode = null)
        {
            return null;
        }

        public async Task<List<string>> GetUserIdsByRoleId(string roleId)
        {
            var result = GetAll().Where(r => r.RoleId == roleId).Select(r=>r.UserId).ToList();
            return await Task.FromResult(result);
        }
    }
}
