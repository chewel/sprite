﻿using CK.Sprite.Cache;
using CK.Sprite.CrossCutting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.ThirdContract
{
    public class LocalSpriteUserCache : BaseLocalCache<SpriteUser>, ISpriteUserCache
    {
        public IRemoteSpriteUserService _remoteSpriteUserService => LazyGetRequiredService(ref remoteSpriteUserService);
        private IRemoteSpriteUserService remoteSpriteUserService;

        public override string CacheKey => ThirdConsts.SpriteUserCacheKey;

        public override List<SpriteUser> GetAll(string applicationCode = null)
        {
            if (!LocalCacheContainer.IsEnabledLocalCache)
            {
                return _remoteSpriteUserService.GetAll();
            }
            else
            {
                return (List<SpriteUser>)LocalCacheContainer.Get(CacheKey, key =>
                {
                    return _remoteSpriteUserService.GetAll();
                });
            }
        }

        public override Dictionary<Guid, SpriteUser> GetAllDict(string applicationCode = null)
        {
            return null;
        }

        public async Task<SpriteUser> GetById(string id)
        {
            var result = GetAll().FirstOrDefault(r => r.Id == id);
            return await Task.FromResult(result);
        }

        public async Task<List<SpriteUser>> GetByIds(IEnumerable<string> ids)
        {
            var result = GetAll().Where(r => ids.Contains(r.Id)).ToList();
            return await Task.FromResult(result);
        }

        public async Task<List<SpriteUser>> GetByIdsAndDeptId(IEnumerable<string> ids, string deptId)
        {
            var result = GetAll().Where(r => ids.Contains(r.Id) && r.MainDeptId == deptId).ToList();
            return await Task.FromResult(result);
        }
    }
}
