﻿namespace CK.Sprite.RabbitMQ.Core
{
    public class XhyzRabbitMqOptions
    {
        public RabbitMqConnections Connections { get; }

        public XhyzRabbitMqOptions()
        {
            Connections = new RabbitMqConnections();
        }
    }
}
