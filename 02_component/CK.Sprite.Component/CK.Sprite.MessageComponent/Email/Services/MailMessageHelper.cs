﻿using System;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;

namespace CK.Sprite.MessageComponent
{
    /// <summary>
    /// 邮件信息
    /// </summary>
    public static class MailMessageHelper
    {
        /// <summary>
        /// 组装邮件文本/附件邮件信息
        /// </summary>
        /// <param name="mailEntity">邮件消息实体</param>
        /// <returns></returns>
        public static MimeMessage AssemblyMailMessage(MailEntity mailEntity)
        {
            if (mailEntity == null)
            {
                throw new ArgumentNullException(nameof(mailEntity));
            }
            var message = new MimeMessage();

            //设置邮件基本信息
            SetMailBaseMessage(message, mailEntity);

            var multipart = new Multipart("mixed");

            //插入文本消息
            if (!string.IsNullOrEmpty(mailEntity.Body))
            {
                var alternative = new MultipartAlternative
                {
                    AssemblyMailTextMessage(mailEntity.Body, mailEntity.MailBodyType)
                 };
                multipart.Add(alternative);
            }

            if(mailEntity.MailFiles != null)
            {
                //插入附件
                foreach (var mailFile in mailEntity.MailFiles)
                {
                    if (mailFile.MailFilePath != null && File.Exists(mailFile.MailFilePath))
                    {
                        var mimePart = AssemblyMailAttachmentMessage(mailFile.MailFileType, mailFile.MailFileSubType,
                             mailFile.MailFilePath);
                        multipart.Add(mimePart);
                    }
                }
            }            

            //组合邮件内容
            message.Body = multipart;
            return message;
        }

        /// <summary>
        /// 设置邮件基础信息
        /// </summary>
        /// <param name="minMessag"></param>
        /// <param name="mailEntity"></param>
        /// <returns></returns>
        public static MimeMessage SetMailBaseMessage(MimeMessage minMessag, MailEntity mailEntity)
        {
            if (minMessag == null)
            {
                throw new ArgumentNullException();
            }
            if (mailEntity == null)
            {
                throw new ArgumentNullException();
            }

            //插入发件人
            minMessag.From.Add(new MailboxAddress(mailEntity.From, mailEntity.FromAddress));

            //插入收件人
            if (mailEntity.Recipients.Any())
            {
                foreach (var recipients in mailEntity.Recipients)
                {
                    minMessag.To.Add(new MailboxAddress(recipients));
                }
            }

            //插入抄送人
            if (mailEntity.Cc != null && mailEntity.Cc.Any())
            {
                foreach (var cC in mailEntity.Cc)
                {
                    minMessag.Cc.Add(new MailboxAddress(cC));
                }
            }

            //插入密送人
            if (mailEntity.Bcc != null && mailEntity.Bcc.Any())
            {
                foreach (var bcc in mailEntity.Bcc)
                {
                    minMessag.Bcc.Add(new MailboxAddress(bcc));
                }
            }

            //插入主题
            minMessag.Subject = mailEntity.Subject;
            return minMessag;
        }

        /// <summary>
        /// 组装邮件文本信息
        /// </summary>
        /// <param name="mailBody">邮件内容</param>
        /// <param name="textPartType">邮件类型(plain,html,rtf,xml)</param>
        /// <returns></returns>
        public static TextPart AssemblyMailTextMessage(string mailBody, TextFormat textPartType)
        {
            if (string.IsNullOrEmpty(mailBody))
            {
                throw new ArgumentNullException();
            }
            //var textBody = new TextPart(textPartType)
            //{
            //    Text = mailBody,
            //};

            //处理查看源文件有乱码问题
            var textBody = new TextPart(textPartType);
            textBody.SetText(Encoding.Default, mailBody);
            return textBody;
        }

        /// <summary>
        /// 组装邮件附件信息
        /// </summary>
        /// <param name="fileAttachmentType">附件类型(image,application)</param>
        /// <param name="fileAttachmentSubType">附件子类型 </param>
        /// <param name="fileAttachmentPath">附件路径</param>
        /// <returns></returns>
        public static MimePart AssemblyMailAttachmentMessage(string fileAttachmentType, string fileAttachmentSubType, string fileAttachmentPath)
        {
            if (string.IsNullOrEmpty(fileAttachmentSubType))
            {
                throw new ArgumentNullException();
            }
            if (string.IsNullOrEmpty(fileAttachmentType))
            {
                throw new ArgumentNullException();
            }
            if (string.IsNullOrEmpty(fileAttachmentPath))
            {
                throw new ArgumentNullException();
            }
            var fileName = Path.GetFileName(fileAttachmentPath);
            var attachment = new MimePart(fileAttachmentType, fileAttachmentSubType)
            {
                Content = new MimeContent(File.OpenRead(fileAttachmentPath)),
                ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                ContentTransferEncoding = ContentEncoding.Base64,
                //FileName = fileName,
            };

            //qq邮箱附件文件名中文乱码问题
            //var charset = "GB18030";
            attachment.ContentType.Parameters.Add(Encoding.Default, "name", fileName);
            attachment.ContentDisposition.Parameters.Add(Encoding.Default, "filename", fileName);

            //处理文件名过长
            foreach (var param in attachment.ContentDisposition.Parameters)
                param.EncodingMethod = ParameterEncodingMethod.Rfc2047;
            foreach (var param in attachment.ContentType.Parameters)
                param.EncodingMethod = ParameterEncodingMethod.Rfc2047;

            return attachment;
        }

        /// <summary>
        /// 创建邮件日志文件
        /// </summary>
        /// <returns></returns>
        public static string CreateMailLog()
        {
            var logPath = AppDomain.CurrentDomain.BaseDirectory + "/DocumentLog/" +
                Guid.NewGuid() + ".txt";

            if (File.Exists(logPath)) return logPath;
            var fs = File.Create(logPath);
            fs.Close();
            return logPath;

        }
    }
}