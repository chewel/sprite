﻿namespace CK.Sprite.MessageComponent
{
    public enum SmtpDeliveryMethod
    {
        Network,
        SpecifiedPickupDirectory
    }
}