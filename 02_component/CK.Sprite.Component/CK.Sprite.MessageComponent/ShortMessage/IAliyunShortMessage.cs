﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Exceptions;
using Aliyun.Acs.Core.Http;
using Aliyun.Acs.Core.Profile;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CK.Sprite.MessageComponent
{
    public interface IAliyunShortMessage
    {
        AlyNoteResponse SendCommonMessage(string phone, Dictionary<string, string> templateParam, string sortMessageTemplateCode);

        AlyNoteResponse SendShortMessage(string phone, string code);
    }
}
