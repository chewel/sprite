﻿using CK.Sprite.Framework;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CK.Sprite.Framework;

namespace CK.Sprite.Form.Test
{
    public class DITest : IDITest, ITransientDependency
    {
        public IServiceProvider ServiceProvider => ServiceLocator.ServiceProvider;

        protected readonly object ServiceProviderLock = new object();

        protected TService LazyGetRequiredService<TService>(ref TService reference)
            => LazyGetRequiredService(typeof(TService), ref reference);

        protected TRef LazyGetRequiredService<TRef>(Type serviceType, ref TRef reference)
        {
            if (reference == null)
            {
                lock (ServiceProviderLock)
                {
                    if (reference == null)
                    {
                        reference = (TRef)ServiceProvider.GetRequiredService(serviceType);
                    }
                }
            }

            return reference;
        }

        public DITest()
        {
        }

        public TenantConfigManager TenantConfigManager => LazyGetRequiredService(ref _tenantConfigManager);
        private TenantConfigManager _tenantConfigManager;

        public string GetString()
        {
            var tenantConfig = TenantConfigManager.GetTenantConfig("Default", "Default");
            return "test";
        }
    }
}
