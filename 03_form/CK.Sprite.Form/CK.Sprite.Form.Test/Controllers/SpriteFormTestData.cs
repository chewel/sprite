﻿using CK.Sprite.Form.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Test.Controllers
{
    public class SpriteFormTestData
    {
        public static SpriteObjectCreateDto GetSpriteObjectCreateDto()
        {
            SpriteObjectCreateDto spriteObjectCreateDto = new SpriteObjectCreateDto()
            {
                ApplicationCode = "Default",
                CreateAudit = true,
                IsTree = false,
                DeleteAudit = true,
                Description = "测试学生表",
                KeyType = EKeyType.Guid,
                ModifyAudit = true,
                Name = "Student",
                ObjectPropertyCreateDtos = new List<ObjectPropertyCreateDto>()
            };

            spriteObjectCreateDto.ObjectPropertyCreateDtos.Add(new ObjectPropertyCreateDto()
            {
                Description = "姓名",
                FieldType = EFieldType.String,
                IsNull = false,
                IsRequired = true,
                IsUnique = true,
                Length = 256,
                Name = "Name",
                Order = "001"
            });

            spriteObjectCreateDto.ObjectPropertyCreateDtos.Add(new ObjectPropertyCreateDto()
            {
                Description = "年龄",
                FieldType = EFieldType.Int,
                IsNull = true,
                IsRequired = false,
                IsUnique = false,
                Name = "Age",
                Order = "002"
            });

            spriteObjectCreateDto.ObjectPropertyCreateDtos.Add(new ObjectPropertyCreateDto()
            {
                Description = "出生年月",
                FieldType = EFieldType.Date,
                IsNull = true,
                IsRequired = false,
                IsUnique = false,
                Name = "Birthday",
                Order = "003"
            });

            //spriteObjectCreateDto.ObjectMethodCreateDtos.Add(new ObjectMethodCreateDto()
            //{
            //    Description = "新增",
            //    MethodExeType = EMethodExeType.Sql,
            //    MethodType = EMethodType.Create,
            //    Name = "CreateStudent",
            //    CommonParamCreateDtos = new List<CommonParamCreateDto>() {
            //        new CommonParamCreateDto() {
            //            Description = "姓名",
            //            IsRequired = true,
            //            ParamDirection = EParamDirection.In,
            //            ParamName = "Name",
            //            ParamType = EParamType.Single,
            //            FieldType = EFieldType.String
            //        },
            //        new CommonParamCreateDto() {
            //            Description = "年龄",
            //            IsRequired = false,
            //            ParamDirection = EParamDirection.In,
            //            ParamName = "Age",
            //            ParamType = EParamType.Single,
            //            FieldType = EFieldType.Int
            //        },
            //        new CommonParamCreateDto() {
            //            Description = "出生年月",
            //            IsRequired = false,
            //            ParamDirection = EParamDirection.In,
            //            ParamName = "Birthday",
            //            ParamType = EParamType.Single,
            //            FieldType = EFieldType.Date
            //        },
            //        new CommonParamCreateDto() {
            //            Description = "新增结果",
            //            ParamDirection = EParamDirection.Out,
            //            ParamType = EParamType.Single,
            //            FieldType = EFieldType.Bool
            //        },
            //    }
            //});

            //spriteObjectCreateDto.ObjectMethodCreateDtos.Add(new ObjectMethodCreateDto()
            //{
            //    Description = "修改",
            //    MethodExeType = EMethodExeType.Sql,
            //    MethodType = EMethodType.Update,
            //    Name = "UpdateStudent",
            //    CommonParamCreateDtos = new List<CommonParamCreateDto>() {
            //        new CommonParamCreateDto() {
            //            Description = "姓名",
            //            IsRequired = true,
            //            ParamDirection = EParamDirection.In,
            //            ParamName = "Name",
            //            ParamType = EParamType.Single,
            //            FieldType = EFieldType.String
            //        },
            //        new CommonParamCreateDto() {
            //            Description = "年龄",
            //            IsRequired = false,
            //            ParamDirection = EParamDirection.In,
            //            ParamName = "Age",
            //            ParamType = EParamType.Single,
            //            FieldType = EFieldType.Int
            //        },
            //        new CommonParamCreateDto() {
            //            Description = "出生年月",
            //            IsRequired = false,
            //            ParamDirection = EParamDirection.In,
            //            ParamName = "Birthday",
            //            ParamType = EParamType.Single,
            //            FieldType = EFieldType.Date
            //        },
            //        new CommonParamCreateDto() {
            //            Description = "修改结果",
            //            ParamDirection = EParamDirection.Out,
            //            ParamType = EParamType.Single,
            //            FieldType = EFieldType.Bool
            //        },
            //    }
            //});

            //spriteObjectCreateDto.ObjectMethodCreateDtos.Add(new ObjectMethodCreateDto()
            //{
            //    Description = "测试复杂方法",
            //    MethodExeType = EMethodExeType.微服务,
            //    MethodType = EMethodType.Execute,
            //    Name = "ExecuteTest",
            //    CommonParamCreateDtos = new List<CommonParamCreateDto>() {
            //        new CommonParamCreateDto() {
            //            Description = "复合参数1",
            //            IsRequired = true,
            //            ParamDirection = EParamDirection.In,
            //            ParamName = "Items1",
            //            ParamType = EParamType.Object,
            //            FieldType = EFieldType.String,
            //            Children = new List<CommonParamCreateDto>()
            //            {
            //                 new CommonParamCreateDto() {
            //                    Description = "复合参数1-1",
            //                    IsRequired = false,
            //                    ParamDirection = EParamDirection.In,
            //                    ParamName = "Items1-1",
            //                    ParamType = EParamType.Single,
            //                    FieldType = EFieldType.String
            //                },
            //                 new CommonParamCreateDto() {
            //                    Description = "复合参数1-2",
            //                    IsRequired = false,
            //                    ParamDirection = EParamDirection.In,
            //                    ParamName = "Items1-2",
            //                    ParamType = EParamType.Single,
            //                    FieldType = EFieldType.String
            //                },
            //                 new CommonParamCreateDto() {
            //                    Description = "复合参数1-3",
            //                    IsRequired = false,
            //                    ParamDirection = EParamDirection.In,
            //                    ParamName = "Items1-3",
            //                    ParamType = EParamType.Object,
            //                    FieldType = EFieldType.String,
            //                    Children = new List<CommonParamCreateDto>()
            //                    {
            //                        new CommonParamCreateDto()
            //                        {
            //                            Description = "复合参数1-3-1",
            //                            IsRequired = false,
            //                            ParamDirection = EParamDirection.In,
            //                            ParamName = "Items1-3-1",
            //                            ParamType = EParamType.Single,
            //                            FieldType = EFieldType.String
            //                        },
            //                        new CommonParamCreateDto()
            //                        {
            //                            Description = "复合参数1-3-2",
            //                            IsRequired = false,
            //                            ParamDirection = EParamDirection.In,
            //                            ParamName = "Items1-3-2",
            //                            ParamType = EParamType.Single,
            //                            FieldType = EFieldType.DateTime
            //                        },
            //                        new CommonParamCreateDto()
            //                        {
            //                            Description = "复合参数1-3-3",
            //                            IsRequired = false,
            //                            ParamDirection = EParamDirection.In,
            //                            ParamName = "Items1-3-3",
            //                            ParamType = EParamType.Single,
            //                            FieldType = EFieldType.Float
            //                        }
            //                    }
            //                },
            //            }
            //        },
            //        new CommonParamCreateDto() {
            //            Description = "复合参数2",
            //            IsRequired = false,
            //            ParamDirection = EParamDirection.In,
            //            ParamName = "Items2",
            //            ParamType = EParamType.Object,
            //            FieldType = EFieldType.Int,
            //            Children = new List<CommonParamCreateDto>()
            //            {
            //                new CommonParamCreateDto()
            //                {
            //                    Description = "复合参数2-1",
            //                    IsRequired = false,
            //                    ParamDirection = EParamDirection.In,
            //                    ParamName = "Items2-1",
            //                    ParamType = EParamType.Single,
            //                    FieldType = EFieldType.String
            //                },
            //                new CommonParamCreateDto()
            //                {
            //                    Description = "复合参数2-2",
            //                    IsRequired = false,
            //                    ParamDirection = EParamDirection.In,
            //                    ParamName = "Items2-2",
            //                    ParamType = EParamType.Single,
            //                    FieldType = EFieldType.Int
            //                },
            //                new CommonParamCreateDto()
            //                {
            //                    Description = "复合参数2-3",
            //                    IsRequired = false,
            //                    ParamDirection = EParamDirection.In,
            //                    ParamName = "Items2-3",
            //                    ParamType = EParamType.List,
            //                    FieldType = EFieldType.Int,
            //                    Children = new List<CommonParamCreateDto>()
            //                    {
            //                        new CommonParamCreateDto()
            //                        {
            //                            Description = "复合参数2-3-1",
            //                            IsRequired = false,
            //                            ParamDirection = EParamDirection.In,
            //                            ParamName = "Items2-3-1",
            //                            ParamType = EParamType.Single,
            //                            FieldType = EFieldType.Int
            //                        },
            //                        new CommonParamCreateDto()
            //                        {
            //                            Description = "复合参数2-3-2",
            //                            IsRequired = false,
            //                            ParamDirection = EParamDirection.In,
            //                            ParamName = "Items2-3-2",
            //                            ParamType = EParamType.Single,
            //                            FieldType = EFieldType.DateTime
            //                        }
            //                    }
            //                }
            //            }
            //        },
            //        new CommonParamCreateDto()
            //        {
            //            Description = "参数3",
            //            IsRequired = false,
            //            ParamDirection = EParamDirection.In,
            //            ParamName = "Param3",
            //            ParamType = EParamType.Single,
            //            FieldType = EFieldType.DateTime
            //        },
            //        new CommonParamCreateDto()
            //        {
            //            Description = "参数4",
            //            IsRequired = false,
            //            ParamDirection = EParamDirection.In,
            //            ParamName = "Param4",
            //            ParamType = EParamType.Single,
            //            FieldType = EFieldType.String
            //        },
            //        new CommonParamCreateDto() {
            //            Description = "输出参数5",
            //            IsRequired = false,
            //            ParamDirection = EParamDirection.In,
            //            ParamName = "OutParam5",
            //            ParamType = EParamType.Object,
            //            FieldType = EFieldType.Int,
            //            Children = new List<CommonParamCreateDto>()
            //            {
            //                new CommonParamCreateDto()
            //                {
            //                    Description = "输出参数5-1",
            //                    IsRequired = false,
            //                    ParamDirection = EParamDirection.Out,
            //                    ParamName = "OutParam5-1",
            //                    ParamType = EParamType.Single,
            //                    FieldType = EFieldType.String
            //                },
            //                new CommonParamCreateDto()
            //                {
            //                    Description = "输出参数5-2",
            //                    IsRequired = false,
            //                    ParamDirection = EParamDirection.Out,
            //                    ParamName = "OutParam5-2",
            //                    ParamType = EParamType.List,
            //                    FieldType = EFieldType.Int,
            //                    Children = new List<CommonParamCreateDto>()
            //                    {
            //                        new CommonParamCreateDto()
            //                        {
            //                            Description = "输出参数5-2-1",
            //                            IsRequired = false,
            //                            ParamDirection = EParamDirection.Out,
            //                            ParamName = "OutParam5-2-1",
            //                            ParamType = EParamType.Single,
            //                            FieldType = EFieldType.Int
            //                        },
            //                        new CommonParamCreateDto()
            //                        {
            //                            Description = "输出参数5-2-2",
            //                            IsRequired = false,
            //                            ParamDirection = EParamDirection.Out,
            //                            ParamName = "OutParam5-2-2",
            //                            ParamType = EParamType.Single,
            //                            FieldType = EFieldType.DateTime
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    }
            //});

            return spriteObjectCreateDto;
        }
    }
}
