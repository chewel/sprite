﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CK.Sprite.Form.Test.Models;
using CK.Sprite.Form.Core;

namespace CK.Sprite.Form.Test.Controllers
{
    [ApiController]
    [Area("spriteform")]
    [ControllerName("SpriteObject")]
    [Route("api/spriteform/spriteobject")]
    public class SpriteObjectController : Controller
    {
        private readonly ISpriteObjectAppService _spriteObjectAppService;
        private readonly SpriteObjectLocalCache _spriteObjectLocalCache;

        public SpriteObjectController(ISpriteObjectAppService spriteObjectAppService, SpriteObjectLocalCache spriteObjectLocalCache)
        {
            _spriteObjectAppService = spriteObjectAppService;
            _spriteObjectLocalCache = spriteObjectLocalCache;
        }

        /// <summary>
        /// 创建SpriteObject
        /// </summary>
        /// <param name="spriteObjectCreateDto">SpriteObject实体</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddSpriteObjectAsync")]
        public async Task AddSpriteObjectAsync(SpriteObjectCreateDto spriteObjectCreateDto)
        {
            await _spriteObjectAppService.AddSpriteObjectAsync(spriteObjectCreateDto);
        }

        [HttpGet]
        [Route("GetListSpriteObjectAsync")]
        public async Task<List<SpriteObjectDto>> GetListSpriteObjectAsync()
        {
            return await _spriteObjectAppService.GetListSpriteObjectAsync();
        }

        [HttpPost]
        [Route("AddObjectProperty")]
        public async Task AddObjectProperty(ObjectPropertyCreateDto objectPropertyCreateDto)
        {
            await _spriteObjectAppService.AddObjectProperty(objectPropertyCreateDto);
        }

        [HttpPost]
        [Route("UpdateObjectProperty")]
        public async Task UpdateObjectProperty(ObjectPropertyUpdateDto objectPropertyUpdateDto)
        {
            await _spriteObjectAppService.UpdateObjectProperty(objectPropertyUpdateDto);
        }

        [HttpGet]
        [Route("DeleteObjectProperty")]
        public async Task DeleteObjectProperty(Guid id)
        {
            await _spriteObjectAppService.DeleteObjectProperty(id);
        }

        [HttpGet]
        [Route("GetListObjectPropertyAsync")]
        public async Task<List<ObjectPropertyDto>> GetListObjectPropertyAsync(Guid objectId)
        {
            return await _spriteObjectAppService.GetListObjectPropertyAsync(objectId);
        }
    }
}
