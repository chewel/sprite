﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CK.Sprite.Form.Test.Models;
using CK.Sprite.Form.Core;
using Newtonsoft.Json.Linq;

namespace CK.Sprite.Form.Test.Controllers
{
    [ApiController]
    [Area("spriteform")]
    [ControllerName("Dict")]
    [Route("api/spriteform/dict")]
    public class DictController : Controller, IDictAppService
    {
        private readonly IDictAppService _dictAppService;

        public DictController(IDictAppService dictAppService)
        {
            _dictAppService = dictAppService;
        }

        [HttpPost]
        [Route("AddDict")]
        public async Task AddDict(DictDto dictDto)
        {
            await _dictAppService.AddDict(dictDto);
        }

        [HttpPost]
        [Route("AddDictItem")]
        public async Task AddDictItem(DictItemDto dictItemDto)
        {
            await _dictAppService.AddDictItem(dictItemDto);
        }

        [HttpGet]
        [Route("DeleteDict")]
        public async Task DeleteDict(Guid id)
        {
            await _dictAppService.DeleteDict(id);
        }

        [HttpGet]
        [Route("DeleteDictItem")]
        public async Task DeleteDictItem(Guid id)
        {
            await _dictAppService.DeleteDictItem(id);
        }

        [HttpPost]
        [Route("UpdateDict")]
        public async Task UpdateDict(DictDto dictDto)
        {
            await _dictAppService.UpdateDict(dictDto);
        }

        [HttpPost]
        [Route("UpdateDictItem")]
        public async Task UpdateDictItem(DictItemDto dictItemDto)
        {
            await _dictAppService.UpdateDictItem(dictItemDto);
        }
    }
}
