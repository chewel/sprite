﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Test
{
    public class WebTestModule : SpriteModule
    {
        public override void PreConfigureServices(IServiceCollection Services)
        {
            Services.AddAssemblyOf<WebTestModule>();

            var configuration = Services.GetConfiguration();
            Configure<DefaultDbConfig>(configuration.GetSection("DefaultDbConfig"));

            Configure<DefaultTenantStoreOptions>(configuration.GetSection("DefaultTenantStoreOptions"));
        }
    }
}
