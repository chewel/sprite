﻿using CK.Sprite.Form.Core;
using CK.Sprite.Framework;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace CK.Sprite.Form.MySql
{
    public class MySqlConnectionProvider : IFormConnectionProvider
    {
        public IDbConnection CreateDbConnection(string strConn)
        {
            return new MySqlConnection(strConn);
        }

        public IAutoBusinessDbService GetAutoBusinessDbService(IUnitOfWork unitOfWork)
        {
            return new MysqlAutoBusinessDb(unitOfWork);
        }

        public T GetRepository<T>(IUnitOfWork unitOfWork) where T:class, IRepository
        {
            if(typeof(T).IsAssignableFrom(typeof(ISpriteObjectRepository)))
            {
                return new SpriteObjectRepository(unitOfWork) as T;
            }

            if (typeof(T).IsAssignableFrom(typeof(IObjectMethodRepository)))
            {
                return new ObjectMethodRepository(unitOfWork) as T;
            }
            
            if (typeof(T).IsAssignableFrom(typeof(IRuntimeRepository)))
            {
                return new MysqlRuntimeRepository(unitOfWork) as T;
            }

            if (typeof(T).IsAssignableFrom(typeof(ISpriteViewRepository)))
            {
                return new SpriteViewRepository(unitOfWork) as T;
            }

            if (typeof(T).IsAssignableFrom(typeof(ISpriteFormRepository)))
            {
                return new SpriteFormRepository(unitOfWork) as T;
            }

            if (typeof(T).IsAssignableFrom(typeof(ISpriteCommonRepository)))
            {
                return new SpriteCommonRepository(unitOfWork) as T;
            }

            return default(T);
        }
    }
}
