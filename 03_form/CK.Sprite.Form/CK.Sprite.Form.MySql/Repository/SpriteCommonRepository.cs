﻿using CK.Sprite.Form.Core;
using CK.Sprite.Framework;
using Dapper;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.MySql
{
    public class SpriteCommonRepository : BaseSpriteRepository<SpriteView>, ISpriteCommonRepository
    {
        public SpriteCommonRepository(IUnitOfWork unitOfWork) : base(unitOfWork) { }

        public async Task CheckDeleteViewOrForm(Guid objId)
        {
            await CheckDelete(objId, "WrapInfos");
            await CheckDelete(objId, "ItemViewCols");
            await CheckDelete(objId, "FormCols");
            await CheckDelete(objId, "SpriteRules");
            await CheckDelete(objId, "RuleActions");
        }

        public async Task<List<T>> GetCommonList<T>(string tableName, List<QueryWhereModel> queryWhereModels, string fields = default)
        {
            JObject paramValues = new JObject();
            string strSql = $"SELECT {(string.IsNullOrEmpty(fields) ? "*" : fields)} FROM {tableName} WHERE {CreateSqlWhere(queryWhereModels, paramValues)};";
            var result = await _unitOfWork.Connection.QueryAsync<T>(strSql, paramValues.ToConventionalDotNetObject());
            return result.ToList();
        }

        public async Task<List<T>> GetCommonList2<T>(string tableName, ExpressSqlModel expressSqlModel, string fields = default)
        {
            JObject paramValues = new JObject();
            string strSql = $"SELECT {(string.IsNullOrEmpty(fields) ? "*" : fields)} FROM {tableName} WHERE {CreateSqlWhere(expressSqlModel, paramValues)};";
            var result = await _unitOfWork.Connection.QueryAsync<T>(strSql, paramValues.ToConventionalDotNetObject());
            return result.ToList();
        }

        public async Task<T> GetDataById<T>(string tableName, Guid id)
        {
            JObject paramValues = new JObject();
            string strSql = $"SELECT * FROM {tableName} WHERE Id=@Id;";
            return await _unitOfWork.Connection.QueryFirstOrDefaultAsync<T>(strSql, new { Id = id});
        }

        private async Task CheckDelete(Guid objId, string tableName)
        {
            string strSql = $"SELECT Id FROM {tableName} WHERE ObjId = @ObjId LIMIT 1;";
            var exsitsId = await _unitOfWork.Connection.QueryFirstOrDefaultAsync<Guid>(strSql, new { ObjId = objId });
            if (!(exsitsId == default || exsitsId == null))
            {
                throw new SpriteException($"删除失败，{tableName}存在相关引用");
            }
        }
    }
}
