﻿using CK.Sprite.Framework;
using System;

namespace CK.Sprite.Form.Core
{
    [Dapper.Contrib.Extensions.Table("FormItems")]
    public class FormItem : GuidEntityBase
    {
        /// <summary>
        /// 表单Id
        /// </summary>
        public Guid FormId { get; set; }

        /// <summary>
        /// 表单项属性设置
        /// </summary>
        public string PropertySettings { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 子对象临时存储（序列化时使用）
        /// </summary>
        [Dapper.Contrib.Extensions.Computed]
        public object Children { get; set; }
    }
}
