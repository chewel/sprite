﻿using CK.Sprite.Framework;
using System;

namespace CK.Sprite.Form.Core
{
    [Dapper.Contrib.Extensions.Table("SpriteCacheMains")]
    public class SpriteCacheMain
    {
        /// <summary>
        /// 业务标识
        /// </summary>
        [Dapper.Contrib.Extensions.ExplicitKey]
        public string Id { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Notes { get; set; }
    }
}
