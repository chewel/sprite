﻿using CK.Sprite.Framework;
using System;

namespace CK.Sprite.Form.Core
{
    [Dapper.Contrib.Extensions.Table("SpriteRules")]
    public class SpriteRule : GuidEntityBase
    {
        /// <summary>
        /// 业务Id
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }

        /// <summary>
        /// 子表单Id
        /// </summary>
        public Guid? SubFormId { get; set; }

        /// <summary>
        /// 子视图Id
        /// </summary>
        public Guid? SubViewId { get; set; }

        /// <summary>
        /// 控件Id(Modal等Wrap对象为控件Id+特定属性标识)
        /// </summary>
        public string ObjId { get; set; }

        /// <summary>
        /// 事件名称
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 子对象临时存储（序列化时使用）
        /// </summary>
        [Dapper.Contrib.Extensions.Computed]
        public object Children { get; set; }
    }
}
