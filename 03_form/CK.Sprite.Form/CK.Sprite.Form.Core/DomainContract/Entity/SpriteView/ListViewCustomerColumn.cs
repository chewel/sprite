﻿using CK.Sprite.Framework;
using System;

namespace CK.Sprite.Form.Core
{
    [Dapper.Contrib.Extensions.Table("ListViewCustomerColumns")]
    public class ListViewCustomerColumn : GuidEntityBase
    {
        /// <summary>
        /// 视图Id
        /// </summary>
        public Guid ViewId { get; set; }

        /// <summary>
        /// 编辑状态列组件
        /// </summary>
        public string EditComponentName { get; set; }

        /// <summary>
        /// 编辑状态组件设置
        /// </summary>
        public string EditSettings { get; set; }

        /// <summary>
        /// 控件名称
        /// </summary>
        public string ComponentName { get; set; }

        /// <summary>
        /// 控件属性
        /// </summary>
        public string ControlSettings { get; set; }

        /// <summary>
        /// 通常为字段名称
        /// </summary>
        public string DataIndex { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
    }
}
