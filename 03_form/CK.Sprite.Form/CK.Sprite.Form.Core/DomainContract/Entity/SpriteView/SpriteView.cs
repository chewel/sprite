﻿using System;

namespace CK.Sprite.Form.Core
{
    [Dapper.Contrib.Extensions.Table("SpriteViews")]
    public class SpriteView : DesignEntityBase
    {
        /// <summary>
        /// 视图英文名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 版本
        /// </summary>
        public Guid Version { get; set; }

        /// <summary>
        /// ItemView=1,ListView=2,TreeView=3
        /// </summary>
        public EViewType ViewType { get; set; }

        /// <summary>
        /// 视图设置
        /// </summary>
        public string PropertySettings { get; set; }

        /// <summary>
        /// 规则定义json存储
        /// </summary>
        public string Rules { get; set; }

        /// <summary>
        /// 弹出框等信息封装Json存储
        /// </summary>
        public string WrapInfos { get; set; }

        /// <summary>
        /// 视图控件设置
        /// </summary>
        public string Controls { get; set; }

        /// <summary>
        /// 扩展1数据json存储(ItemView为viewrow,ListView为filter信息)
        /// </summary>
        public string Extension1s { get; set; }

        /// <summary>
        /// 扩展2数据json存储，(ListView为ViewColumn信息)
        /// </summary>
        public string Extension2s { get; set; }
        public string Extension3s { get; set; }
        public string Extension4s { get; set; }
    }
}
