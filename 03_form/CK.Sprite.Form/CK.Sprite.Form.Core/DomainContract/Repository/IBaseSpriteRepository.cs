﻿using CK.Sprite.Framework;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    /// <summary>
    /// 通用方法
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseSpriteRepository<T> : IGuidRepository<T> where T : class
    {
        /// <summary>
        /// 获取特定实体ApplicationCode
        /// </summary>
        /// <param name="tableName">表名称</param>
        /// <param name="id">实体Id</param>
        /// <returns></returns>
        Task<string> GetApplicationCodeAsync(string tableName, Guid id);
    }
}
