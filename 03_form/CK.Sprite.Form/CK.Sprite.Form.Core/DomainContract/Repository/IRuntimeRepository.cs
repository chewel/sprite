﻿using CK.Sprite.Framework;
using Dapper.Contrib.Extensions;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface IRuntimeRepository : IRepository
    {
        /// <summary>
        /// 执行默认插入方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">客户端传递的参数</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns>插入主键{Result:result}</returns>
        Task<JObject> DoDefaultCreateMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认修改方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">客户端传递的参数</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns>修改结果受影响行数，{Result:result}</returns>
        Task<JObject> DoDefaultUpdateMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认删除方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">客户端传递的参数(只有Id)</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns>删除受影响行数，{Result:result}</returns>
        Task<JObject> DoDefaultDeleteMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认Get方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">客户端传递的参数(只有Id)</param>
        /// <param name="fields">字段集合</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns>Get结果对象，{Result:result}</returns>
        Task<JObject> DoDefaultGetMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JArray fields, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认List方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="fields">字段集合</param>
        /// <param name="orderbys">排序</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns>List结果对象，{Result:result}</returns>
        Task<JObject> DoDefaultListMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JArray fields, JToken orderbys, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认Update Where方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">客户端传递的参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns></returns>
        Task<JObject> DoDefaultUpdateWhereMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JToken sqlWheres, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认Delete Where方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        Task<JObject> DoDefaultDeleteWhereMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JToken sqlWheres, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认Get Where方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="fields">字段集合</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        Task<JObject> DoDefaultGetWhereMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JToken sqlWheres, JArray fields, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认List Where方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="fields">字段集合</param>
        /// <param name="orderbys">排序参数</param>
        /// <returns>List Where结果对象，{Result:result}</returns>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        Task<JObject> DoDefaultListWhereMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JToken sqlWheres, JArray fields, JToken orderbys, string sqlMethodContent = "");

        /// <summary>
        /// 执行默认Page List方法
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="fields">字段集合</param>
        /// <param name="orderbys">排序参数</param>
        /// <param name="maxResultCount">最大行数</param>
        /// <param name="skipCount">跳过行数</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns></returns>
        Task<JObject> DoDefaultPageListMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JToken sqlWheres, JArray fields, JToken orderbys, JToken maxResultCount, JToken skipCount, string sqlMethodContent = "");

        /// <summary>
        /// 通用方法执行
        /// </summary>
        /// <param name="spriteObjectDto">object对象</param>
        /// <param name="paramValues">方法参数</param>
        /// <param name="sqlWheres">参数化Where条件</param>
        /// <param name="fields">字段集合</param>
        /// <param name="orderbys">排序参数</param>
        /// <param name="sqlMethodContent">自定义方法执行内容</param>
        /// <returns></returns>
        Task<JObject> DoSqlMethodAsync(SpriteObjectDto spriteObjectDto, JObject paramValues, JToken sqlWheres, JArray fields, JToken orderbys, string sqlMethodContent);
    }
}
