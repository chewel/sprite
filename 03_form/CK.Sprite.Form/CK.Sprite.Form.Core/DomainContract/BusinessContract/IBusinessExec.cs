﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface IBusinessExec
    {
        /// <summary>
        /// 执行业务方法接口
        /// </summary>
        /// <param name="execParams">参数信息{ApplicationCode,TenantCode,Params}</param>
        /// <param name="unitOfWork">事物执行的UnitOfWork</param>
        /// <returns></returns>
        Task<JObject> ExecBusinessMethod(JObject execParams, IUnitOfWork unitOfWork = null);
    }
}
