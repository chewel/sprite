﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class SpriteFormAppService : AppService, ISpriteFormAppService
    {
        private readonly SpriteFormService _spriteFormService;
        public CommonService _commonService => LazyGetRequiredService(ref commonService);
        private CommonService commonService;
        public SpriteFormAppService(SpriteFormService spriteFormService)
        {
            _spriteFormService = spriteFormService;
        }

        #region SpriteForm Operate

        public async Task AddSpriteFormAsync(SpriteFormCreateDto spriteFormCreateDto)
        {
            await _spriteFormService.AddSpriteFormAsync(spriteFormCreateDto);
        }

        public async Task UpdateSpriteFormAsync(SpriteFormUpdateDto spriteFormUpdateDto)
        {
            await _spriteFormService.UpdateSpriteFormAsync(spriteFormUpdateDto);
        }

        public async Task DeleteSpriteForm(Guid id)
        {
            await _spriteFormService.DeleteSpriteForm(id);
        }

        public async Task<List<SpriteFormDto>> GetSpriteFormAsync(bool isTemplate, string applicationCode = "Default", int? formType = null, string filter = default)
        {
            var results = await _spriteFormService.GetSpriteFormAsync(isTemplate, applicationCode, formType, filter);
            return Mapper.Map<List<SpriteFormDto>>(results);
        }

        public async Task SetFormTemplate(Guid id, bool isTemplate)
        {
            await _spriteFormService.SetFormTemplate(id, isTemplate);
        }

        public async Task<SpriteFormDto> GetSpriteFormByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<SpriteForm>("SpriteForms", id);
            return Mapper.Map<SpriteFormDto>(dbData);
        }

        #endregion

        #region FormItem Operate

        public async Task AddFormItem(FormItemCreateDto formItemCreateDto)
        {
            await _spriteFormService.AddFormItem(formItemCreateDto);
        }

        public async Task UpdateFormItem(FormItemUpdateDto formItemUpdateDto)
        {
            await _spriteFormService.UpdateFormItem(formItemUpdateDto);
        }

        public async Task DeleteFormItem(Guid id)
        {
            await _spriteFormService.DeleteFormItem(id);
        }

        public async Task<List<FormItemDto>> GetListFormItemAsync(Guid formId)
        {
            var results = await _spriteFormService.GetListFormItemAsync(formId);
            return Mapper.Map<List<FormItemDto>>(results);
        }

        public async Task<FormItemDto> GetFormItemByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<FormItem>("FormItems", id);
            return Mapper.Map<FormItemDto>(dbData);
        }

        #endregion

        #region FormRow Operate

        public async Task AddFormRow(FormRowCreateDto formRowCreateDto)
        {
            await _spriteFormService.AddFormRow(formRowCreateDto);
        }

        public async Task UpdateFormRow(FormRowUpdateDto formRowUpdateDto)
        {
            await _spriteFormService.UpdateFormRow(formRowUpdateDto);
        }

        public async Task DeleteFormRow(Guid id)
        {
            await _spriteFormService.DeleteFormRow(id);
        }

        public async Task<List<FormRowDto>> GetListFormRowAsync(Guid formItemId)
        {
            var results = await _spriteFormService.GetListFormRowAsync(formItemId);
            return Mapper.Map<List<FormRowDto>>(results);
        }

        public async Task<FormRowDto> GetFormRowByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<FormRow>("FormRows", id);
            return Mapper.Map<FormRowDto>(dbData);
        }

        #endregion

        #region FormCol Operate

        public async Task AddFormCol(FormColCreateDto formColCreateDto)
        {
            await _spriteFormService.AddFormCol(formColCreateDto);
        }

        public async Task UpdateFormCol(FormColUpdateDto formColUpdateDto)
        {
            await _spriteFormService.UpdateFormCol(formColUpdateDto);
        }

        public async Task DeleteFormCol(Guid id)
        {
            await _spriteFormService.DeleteFormCol(id);
        }

        public async Task<List<FormColDto>> GetListFormColAsync(Guid formRowId)
        {
            var results = await _spriteFormService.GetListFormColAsync(formRowId);
            return Mapper.Map<List<FormColDto>>(results);
        }

        public async Task<FormColDto> GetFormColByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<FormCol>("FormCols", id);
            return Mapper.Map<FormColDto>(dbData);
        }

        #endregion
    }
}
