﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class DictAppService : AppService, IDictAppService
    {
        private readonly DictService _dictService;
        public DictAppService(DictService dictService)
        {
            _dictService = dictService;
        }

        public async Task AddDict(DictDto dictDto)
        {
            await _dictService.AddDict(dictDto);
        }

        public async Task UpdateDict(DictDto dictDto)
        {
            await _dictService.UpdateDict(dictDto);
        }

        public async Task DeleteDict(Guid id)
        {
            await _dictService.DeleteDict(id);
        }

        public async Task AddDictItem(DictItemDto dictItemDto)
        {
            await _dictService.AddDictItem(dictItemDto);
        }

        public async Task UpdateDictItem(DictItemDto dictItemDto)
        {
            await _dictService.UpdateDictItem(dictItemDto);
        }

        public async Task DeleteDictItem(Guid id)
        {
            await _dictService.DeleteDictItem(id);
        }
    }
}
