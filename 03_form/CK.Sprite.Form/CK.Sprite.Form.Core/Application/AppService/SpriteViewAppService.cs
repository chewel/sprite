﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class SpriteViewAppService : AppService, ISpriteViewAppService
    {
        private readonly SpriteViewService _spriteViewService;

        public CommonService _commonService => LazyGetRequiredService(ref commonService);
        private CommonService commonService;

        public SpriteViewAppService(SpriteViewService spriteViewService)
        {
            _spriteViewService = spriteViewService;
        }

        #region SpriteView Operate

        public async Task AddSpriteViewAsync(SpriteViewCreateDto spriteViewCreateDto)
        {
            await _spriteViewService.AddSpriteViewAsync(spriteViewCreateDto);
        }

        public async Task UpdateSpriteViewAsync(SpriteViewUpdateDto spriteViewUpdateDto)
        {
            await _spriteViewService.UpdateSpriteViewAsync(spriteViewUpdateDto);
        }

        public async Task DeleteSpriteView(Guid id)
        {
            await _spriteViewService.DeleteSpriteView(id);
        }

        public async Task<List<SpriteViewDto>> GetSpriteViewAsync(string applicationCode = "Default", int? viewType = null, string filter = default)
        {
            var results = await _spriteViewService.GetSpriteViewAsync(applicationCode, viewType, filter);
            return Mapper.Map<List<SpriteViewDto>>(results);
        }

        public async Task<SpriteViewDto> GetSpriteViewByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<SpriteView>("SpriteViews", id);
            return Mapper.Map<SpriteViewDto>(dbData);
        }

        #endregion

        #region ListViewFilter Operate

        public async Task AddListViewFilter(ListViewFilterCreateDto listViewFilterCreateDto)
        {
            await _spriteViewService.AddListViewFilter(listViewFilterCreateDto);
        }

        public async Task UpdateListViewFilter(ListViewFilterUpdateDto listViewFilterUpdateDto)
        {
            await _spriteViewService.UpdateListViewFilter(listViewFilterUpdateDto);
        }

        public async Task DeleteListViewFilter(Guid id)
        {
            await _spriteViewService.DeleteListViewFilter(id);
        }

        public async Task<List<ListViewFilterDto>> GetListListViewFilterAsync(Guid viewId)
        {
            var results = await _spriteViewService.GetListListViewFilterAsync(viewId);
            return Mapper.Map<List<ListViewFilterDto>>(results);
        }

        public async Task<ListViewFilterDto> GetListViewFilterByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<ListViewFilter>("ListViewFilters", id);
            return Mapper.Map<ListViewFilterDto>(dbData);
        }

        #endregion

        #region ListViewCustomerColumn Operate

        public async Task AddListViewCustomerColumn(ListViewCustomerColumnCreateDto listViewCustomerColumnCreateDto)
        {
            await _spriteViewService.AddListViewCustomerColumn(listViewCustomerColumnCreateDto);
        }

        public async Task UpdateListViewCustomerColumn(ListViewCustomerColumnUpdateDto listViewCustomerColumnUpdateDto)
        {
            await _spriteViewService.UpdateListViewCustomerColumn(listViewCustomerColumnUpdateDto);
        }

        public async Task DeleteListViewCustomerColumn(Guid id)
        {
            await _spriteViewService.DeleteListViewCustomerColumn(id);
        }

        public async Task<List<ListViewCustomerColumnDto>> GetListListViewCustomerColumnAsync(Guid viewId)
        {
            var results = await _spriteViewService.GetListListViewCustomerColumnAsync(viewId);
            return Mapper.Map<List<ListViewCustomerColumnDto>>(results);
        }

        public async Task<ListViewCustomerColumnDto> GetListViewCustomerColumnByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<ListViewCustomerColumn>("ListViewCustomerColumns", id);
            return Mapper.Map<ListViewCustomerColumnDto>(dbData);
        }

        #endregion

        #region ItemViewRow Operate

        public async Task AddItemViewRow(ItemViewRowCreateDto itemViewRowCreateDto)
        {
            await _spriteViewService.AddItemViewRow(itemViewRowCreateDto);
        }

        public async Task UpdateItemViewRow(ItemViewRowUpdateDto itemViewRowUpdateDto)
        {
            await _spriteViewService.UpdateItemViewRow(itemViewRowUpdateDto);
        }

        public async Task DeleteItemViewRow(Guid id)
        {
            await _spriteViewService.DeleteItemViewRow(id);
        }

        public async Task<List<ItemViewRowDto>> GetListItemViewRowAsync(Guid viewId)
        {
            var results = await _spriteViewService.GetListItemViewRowAsync(viewId);
            return Mapper.Map<List<ItemViewRowDto>>(results);
        }

        public async Task<ItemViewRowDto> GetItemViewRowByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<ItemViewRow>("ItemViewRows", id);
            return Mapper.Map<ItemViewRowDto>(dbData);
        }

        #endregion

        #region ItemViewCol Operate

        public async Task AddItemViewCol(ItemViewColCreateDto itemViewColCreateDto)
        {
            await _spriteViewService.AddItemViewCol(itemViewColCreateDto);
        }

        public async Task UpdateItemViewCol(ItemViewColUpdateDto itemViewColUpdateDto)
        {
            await _spriteViewService.UpdateItemViewCol(itemViewColUpdateDto);
        }

        public async Task DeleteItemViewCol(Guid id)
        {
            await _spriteViewService.DeleteItemViewCol(id);
        }

        public async Task<List<ItemViewColDto>> GetListItemViewColAsync(Guid itemViewRowId)
        {
            var results = await _spriteViewService.GetListItemViewColAsync(itemViewRowId);
            return Mapper.Map<List<ItemViewColDto>>(results);
        }

        public async Task<ItemViewColDto> GetItemViewColByIdAsync(Guid id)
        {
            var dbData = await _commonService.GetDataById<ItemViewCol>("ItemViewCols", id);
            return Mapper.Map<ItemViewColDto>(dbData);
        }

        #endregion
    }
}
