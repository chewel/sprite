﻿using AutoMapper;
using JetBrains.Annotations;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class AutomapperConfig : Profile
    {
        public AutomapperConfig()
        {
            #region SpriteObject

            CreateMap<ObjectPropertyDto, ObjectProperty>(MemberList.None);
            CreateMap<ObjectProperty, ObjectPropertyDto>(MemberList.None);
            CreateMap<ObjectPropertyCreateDto, ObjectPropertyDto>(MemberList.None);
            CreateMap<ObjectPropertyCreateDto, ObjectProperty>(MemberList.None);
            CreateMap<ObjectPropertyUpdateDto, ObjectProperty>(MemberList.None);
            CreateMap<ObjectPropertyUpdateDto, ObjectPropertyDto>(MemberList.None);

            CreateMap<SpriteObjectDto, SpriteObject>(MemberList.None);
            CreateMap<SpriteObject, SpriteObjectDto>(MemberList.None);
            CreateMap<SpriteObjectCreateDto, SpriteObject>(MemberList.None);
            CreateMap<SpriteObjectCreateDto, SpriteObjectDto>(MemberList.None);
            CreateMap<SpriteObjectUpdateDto, SpriteObject>(MemberList.None);
            CreateMap<SpriteObjectUpdateDto, SpriteObjectDto>(MemberList.None);

            #endregion

            #region SpriteView

            CreateMap<SpriteView, SpriteViewDto>(MemberList.None);
            CreateMap<SpriteViewCreateDto, SpriteView>(MemberList.None);
            CreateMap<SpriteViewUpdateDto, SpriteView>(MemberList.None);

            CreateMap<ListViewFilter, ListViewFilterDto>(MemberList.None);
            CreateMap<ListViewFilterCreateDto, ListViewFilter>(MemberList.None);
            CreateMap<ListViewFilterUpdateDto, ListViewFilter>(MemberList.None);

            CreateMap<ListViewCustomerColumn, ListViewCustomerColumnDto>(MemberList.None);
            CreateMap<ListViewCustomerColumnCreateDto, ListViewCustomerColumn>(MemberList.None);
            CreateMap<ListViewCustomerColumnUpdateDto, ListViewCustomerColumn>(MemberList.None);

            CreateMap<ItemViewRow, ItemViewRowDto>(MemberList.None);
            CreateMap<ItemViewRowCreateDto, ItemViewRow>(MemberList.None);
            CreateMap<ItemViewRowUpdateDto, ItemViewRow>(MemberList.None);

            CreateMap<ItemViewCol, ItemViewColDto>(MemberList.None);
            CreateMap<ItemViewColCreateDto, ItemViewCol>(MemberList.None);
            CreateMap<ItemViewColUpdateDto, ItemViewCol>(MemberList.None);

            #endregion

            #region SpriteForm

            CreateMap<SpriteForm, SpriteFormDto>(MemberList.None);
            CreateMap<SpriteFormCreateDto, SpriteForm>(MemberList.None);
            CreateMap<SpriteFormUpdateDto, SpriteForm>(MemberList.None);

            CreateMap<FormItem, FormItemDto>(MemberList.None);
            CreateMap<FormItemCreateDto, FormItem>(MemberList.None);
            CreateMap<FormItemUpdateDto, FormItem>(MemberList.None);

            CreateMap<FormRow, FormRowDto>(MemberList.None);
            CreateMap<FormRowCreateDto, FormRow>(MemberList.None);
            CreateMap<FormRowUpdateDto, FormRow>(MemberList.None);

            CreateMap<FormCol, FormColDto>(MemberList.None);
            CreateMap<FormColCreateDto, FormCol>(MemberList.None);
            CreateMap<FormColUpdateDto, FormCol>(MemberList.None);

            #endregion

            #region Common

            CreateMap<WrapInfo, WrapInfoDto>(MemberList.None);
            CreateMap<WrapInfoCreateDto, WrapInfo>(MemberList.None);
            CreateMap<WrapInfoUpdateDto, WrapInfo>(MemberList.None);

            CreateMap<Control, ControlDto>(MemberList.None);
            CreateMap<ControlCreateDto, Control>(MemberList.None);
            CreateMap<ControlUpdateDto, Control>(MemberList.None);

            CreateMap<SpriteRule, SpriteRuleDto>(MemberList.None);
            CreateMap<SpriteRuleCreateDto, SpriteRule>(MemberList.None);
            CreateMap<SpriteRuleUpdateDto, SpriteRule>(MemberList.None);

            CreateMap<RuleAction, RuleActionDto>(MemberList.None);
            CreateMap<RuleActionCreateDto, RuleAction>(MemberList.None);
            CreateMap<RuleActionUpdateDto, RuleAction>(MemberList.None);

            CreateMap<CommonParam, CommonParamDto>(MemberList.None);
            CreateMap<CommonParamCreateDto, CommonParam>(MemberList.None);
            CreateMap<CommonParamUpdateDto, CommonParam>(MemberList.None);

            CreateMap<ObjectMethod, ObjectMethodDto>(MemberList.None);
            CreateMap<ObjectMethodCreateDto, ObjectMethod>(MemberList.None);
            CreateMap<ObjectMethodUpdateDto, ObjectMethod>(MemberList.None);

            #endregion

            #region Framework

            CreateMap<Dict, DictDto>(MemberList.None);
            CreateMap<DictDto, Dict>(MemberList.None);

            CreateMap<DictItem, DictItemDto>(MemberList.None);
            CreateMap<DictItemDto, DictItem>(MemberList.None);
            CreateMap<UpdateDictItemDto, DictItem>(MemberList.None);


            #endregion
        }
    }
}
