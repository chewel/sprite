﻿using JetBrains.Annotations;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class CommonConsts
    {
        public const string SpriteObjectCacheKey = "SpriteObject_Cache";
        public const string ObjectMethodCacheKey = "ObjectMethod_Cache";
        public const string SpriteViewCacheKey = "SpriteView_Cache";
        public const string SpriteFormCacheKey = "SpriteForm_Cache";

        public const string FrameworkDictCacheKey = "Framework_Dict_Cache";

        public const string SpriteViewTableName = "SpriteViews";
        public const string SpriteObjectTableName = "SpriteObjects";
        public const string SpriteFormTableName = "SpriteForms";
        public const string ObjectMethodTableName = "ObjectMethods";
    }
}
