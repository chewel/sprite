﻿using JetBrains.Annotations;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    /// <summary>
    /// 主键类型
    /// </summary>
    public enum EKeyType
    {
        Int = 1,
        Guid = 2
    }

    /// <summary>
    /// 字段类型
    /// </summary>
    public enum EFieldType
    {
        String = 1,
        Int = 2,
        Bool = 3,
        Date = 4,
        DateTime = 5,
        AutoNumber = 6,
        AutoGuid = 7,
        Guid = 8,
        Text = 9,
        Float = 10,
        Decimal = 11
    }

    /// <summary>
    /// 方法类型
    /// </summary>
    public enum EMethodType
    {
        Create = 1,
        Update = 2,
        Delete = 3,
        Get = 4,
        List = 5,
        PageList = 6,
        Execute = 7,
        CreateRange = 8,
        UpdateWhere = 9,
        DeleteWhere = 10,
        GetWhere = 11,
        ListWhere = 12
    }

    /// <summary>
    /// 方法执行类型
    /// </summary>
    public enum EMethodExeType
    {
        Sql = 1,
        反射 = 2,
        微服务 = 3
    }
}
