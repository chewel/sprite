﻿using JetBrains.Annotations;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    /// <summary>
    /// 视图类型
    /// </summary>
    public enum EViewType
    {
        ItemView = 1,
        ListView = 2,
        TreeView = 3
    }

    /// <summary>
    /// 过滤条件类型
    /// </summary>
    public enum EFilterType
    {
        隐藏查询条件 = 1,
        常规查询 = 2,
        高级查询 = 3
    }
}
