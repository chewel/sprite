﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CK.Sprite.Form.Core
{
    [Serializable]
    public class SpriteObjectDto : SpriteObjectUpdateDto
    {
        /// <summary>
        /// 版本，新增不需要填
        /// </summary>
        public Guid Version { get; set; }

        public string ApplicationCode { get; set; }

        /// <summary>
        /// 表名，新增后不能修改
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 主键类型（业务系统原则都用Guid）
        /// </summary>
        public EKeyType KeyType { get; set; }

        /// <summary>
        /// 是否为树(PId,TreeCode,Path,Icon,Title)
        /// </summary>
        public bool IsTree { get; set; }

        /// <summary>
        /// 创建审计(CreatorId，CreationTime)
        /// </summary>
        public bool CreateAudit { get; set; }

        /// <summary>
        /// 修改审计(LastModifierId，LastModificationTime)
        /// </summary>
        public bool ModifyAudit { get; set; }

        /// <summary>
        /// 删除审计(DeleterId，IsDeleted，DeletionTime)
        /// </summary>
        public bool DeleteAudit { get; set; }

        [JsonIgnore]
        /// <summary>
        /// 属性冗余存储
        /// </summary>
        public List<ObjectPropertyDto> ObjectPropertyDtos { get; set; }
    }

    [Serializable]
    public class SpriteObjectCreateDto : SpriteObjectUpdateDto
    {
        public string ApplicationCode { get; set; }

        /// <summary>
        /// 表名，新增后不能修改
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 主键类型（业务系统原则都用Guid）
        /// </summary>
        public EKeyType KeyType { get; set; }

        /// <summary>
        /// 是否为树(PId,TreeCode,Path,Icon,Title)
        /// </summary>
        public bool IsTree { get; set; }

        /// <summary>
        /// 创建审计(新增人Id，新增时间)
        /// </summary>
        public bool CreateAudit { get; set; }

        /// <summary>
        /// 修改审计(修改人Id，修改时间)
        /// </summary>
        public bool ModifyAudit { get; set; }

        /// <summary>
        /// 删除审计(是否删除，删除人Id，删除时间)
        /// </summary>
        public bool DeleteAudit { get; set; }

        /// <summary>
        /// 属性冗余存储
        /// </summary>
        public List<ObjectPropertyCreateDto> ObjectPropertyCreateDtos { get; set; }
    }

    [Serializable]
    public class SpriteObjectUpdateDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
    }
}
