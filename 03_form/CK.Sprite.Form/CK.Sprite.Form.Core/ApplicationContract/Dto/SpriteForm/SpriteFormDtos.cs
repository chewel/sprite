﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Serializable]
    public class SpriteFormVueDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 表单英文名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 表单设置
        /// </summary>
        public object PropertySettings { get; set; }
        public string ApplicationCode { get; set; }
        /// <summary>
        /// 版本，新增不需要填
        /// </summary>
        public Guid Version { get; set; }

        /// <summary>
        /// NormalForm=1,TabForm=2,StepForm=3
        /// </summary>
        public EFormType FormType { get; set; }

        public object RelationInfos { get; set; }
        public object Rules { get; set; }
        public object WrapInfos { get; set; }
        public object Controls { get; set; }
        public object FormItems { get; set; }

        /// <summary>
        /// 从菜单进入Wrap配置信息
        /// </summary>
        public object MenuWrapConfigs { get; set; }
    }

    [Serializable]
    public class SpriteFormDto : SpriteFormUpdateDto
    {
        public string ApplicationCode { get; set; }
        /// <summary>
        /// 版本，新增不需要填
        /// </summary>
        public Guid Version { get; set; }

        /// <summary>
        /// NormalForm=1,TabForm=2,StepForm=3
        /// </summary>
        public EFormType FormType { get; set; }

        public bool IsTemplate { get; set; }
    }

    [Serializable]
    public class SpriteFormCreateDto : SpriteFormUpdateDto
    {
        public string ApplicationCode { get; set; }

        /// <summary>
        /// NormalForm=1,TabForm=2,StepForm=3
        /// </summary>
        public EFormType FormType { get; set; }
    }

    [Serializable]
    public class SpriteFormUpdateDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 表单英文名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 表单设置
        /// </summary>
        public string PropertySettings { get; set; }

        /// <summary>
        /// 从菜单进入Wrap配置信息
        /// </summary>
        public string MenuWrapConfigs { get; set; }
    }
}
