﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Serializable]
    public class SpriteViewVueDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 视图英文名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 视图属性设置
        /// </summary>
        public object PropertySettings { get; set; }

        public string ApplicationCode { get; set; }
        /// <summary>
        /// 版本，新增不需要填
        /// </summary>
        public Guid Version { get; set; }

        /// <summary>
        /// ItemView=1,ListView=2,TreeView=3
        /// </summary>
        public EViewType ViewType { get; set; }

        public object RelationInfos { get; set; }
        public object Rules { get; set; }
        public object WrapInfos { get; set; }
        public object Controls { get; set; }
        public object Extension1s { get; set; }
        public object Extension2s { get; set; }
    }

    public class RelasionInfo
    {
        /// <summary>
        /// 1=表单,2=视图
        /// </summary>
        public int RelationType { get; set; }

        /// <summary>
        /// 表单或者视图Id
        /// </summary>
        public Guid Id { get; set; }
    }

    [Serializable]
    public class SpriteViewDto : SpriteViewUpdateDto
    {
        public string ApplicationCode { get; set; }
        /// <summary>
        /// 版本，新增不需要填
        /// </summary>
        public Guid Version { get; set; }

        /// <summary>
        /// ItemView=1,ListView=2,TreeView=3
        /// </summary>
        public EViewType ViewType { get; set; }
    }

    [Serializable]
    public class SpriteViewCreateDto : SpriteViewUpdateDto
    {
        public string ApplicationCode { get; set; }

        /// <summary>
        /// ItemView=1,ListView=2,TreeView=3
        /// </summary>
        public EViewType ViewType { get; set; }
    }

    [Serializable]
    public class SpriteViewUpdateDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 视图英文名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 视图属性设置
        /// </summary>
        public string PropertySettings { get; set; }
    }
}
