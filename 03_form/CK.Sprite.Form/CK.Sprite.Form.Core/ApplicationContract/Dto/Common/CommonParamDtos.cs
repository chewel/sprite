﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Serializable]
    public class CommonParamDto : CommonParamUpdateDto
    {
        /// <summary>
        /// 方法Id外键
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }

        /// <summary>
        /// Object外键
        /// </summary>
        public Guid ObjectId { get; set; }

        /// <summary>
        /// 方法名称关联（Param1:Param2），需要包含自身
        /// </summary>
        public string TreePath { get; set; }
    }

    [Serializable]
    public class CommonParamCreateDto : CommonParamUpdateDto
    {
        /// <summary>
        /// 方法Id外键
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }

        /// <summary>
        /// Object外键
        /// </summary>
        public Guid ObjectId { get; set; }
        /// <summary>
        /// 方法名称关联（Param1:Param2），需要包含自身，新增时忽略
        /// </summary>
        public string TreePath { get; set; }

        public List<CommonParamCreateDto> Children { get; set; }
    }

    /// <summary>
    /// 修改只支持单条记录修改
    /// </summary>
    [Serializable]
    public class CommonParamUpdateDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 参数名称
        /// </summary>
        public string ParamName { get; set; }

        /// <summary>
        /// 参数类型
        /// </summary>
        public EFieldType FieldType { get; set; }

        /// <summary>
        /// 参数值类型
        /// </summary>
        public EParamType ParamType { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 是否必填
        /// </summary>
        public bool IsRequired { get; set; }

        /// <summary>
        /// 参数长度
        /// </summary>
        public int Length { get; set; }

        /// <summary>
        /// 正则表达式验证
        /// </summary>
        public string ValidateRegular { get; set; }

        /// <summary>
        /// 验证类型
        /// </summary>
        public EValidateType ValidateType { get; set; }

        /// <summary>
        /// 输入或者输出类型
        /// </summary>
        public EParamDirection ParamDirection { get; set; }
    }
}
