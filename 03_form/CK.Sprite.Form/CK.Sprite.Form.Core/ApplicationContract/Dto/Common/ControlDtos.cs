﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Serializable]
    public class ControlDto : ControlUpdateDto
    {
        /// <summary>
        /// 业务Id
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }

        /// <summary>
        /// ListView视图方法执行=1,ListView查询按钮 = 2,ListView操作方法执行 = 3,ListView列表更多方法执行 = 4
        /// </summary>
        public EOperateType OperateType { get; set; }
    }

    [Serializable]
    public class ControlCreateDto : ControlUpdateDto
    {
        /// <summary>
        /// 业务Id
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }

        /// <summary>
        /// ListView视图方法执行=1,ListView查询按钮 = 2,ListView操作方法执行 = 3,ListView列表更多方法执行 = 4
        /// </summary>
        public EOperateType OperateType { get; set; }
    }

    [Serializable]
    public class ControlUpdateDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 控件名称
        /// </summary>
        public string ComponentName { get; set; }

        /// <summary>
        /// 控件属性
        /// </summary>
        public string ControlSettings { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public string Order { get; set; }
    }
}
