﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    [Serializable]
    public class SpriteRuleDto : SpriteRuleUpdateDto
    {
        /// <summary>
        /// 业务Id
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }
    }

    [Serializable]
    public class SpriteRuleCreateDto : SpriteRuleUpdateDto
    {
        /// <summary>
        /// 业务Id
        /// </summary>
        public Guid BusinessId { get; set; }

        /// <summary>
        /// 业务分类，Object,Form,View等
        /// </summary>
        public string BusinessCategory { get; set; }
    }

    [Serializable]
    public class SpriteRuleUpdateDto
    {
        /// <summary>
        /// 实体主键，新增时不需要赋值
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 子表单Id
        /// </summary>
        public Guid? SubFormId { get; set; }

        /// <summary>
        /// 子视图Id
        /// </summary>
        public Guid? SubViewId { get; set; }

        /// <summary>
        /// 控件Id(Modal等Wrap对象为控件Id+特定属性标识)
        /// </summary>
        public string ObjId { get; set; }

        /// <summary>
        /// 事件名称
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

    }
}
