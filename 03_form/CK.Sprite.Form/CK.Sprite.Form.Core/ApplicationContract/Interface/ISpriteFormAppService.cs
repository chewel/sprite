﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface ISpriteFormAppService : IAppService
    {
        #region SpriteForm Operate

        Task AddSpriteFormAsync(SpriteFormCreateDto spriteFormCreateDto);

        Task UpdateSpriteFormAsync(SpriteFormUpdateDto spriteFormUpdateDto);

        Task DeleteSpriteForm(Guid id);

        Task<List<SpriteFormDto>> GetSpriteFormAsync(bool isTemplate, string applicationCode = "Default", int? formType = null, string filter = default);

        Task SetFormTemplate(Guid id, bool isTemplate);

        Task<SpriteFormDto> GetSpriteFormByIdAsync(Guid id);

        #endregion

        #region FormItem Operate

        Task AddFormItem(FormItemCreateDto formItemCreateDto);

        Task UpdateFormItem(FormItemUpdateDto formItemUpdateDto);

        Task DeleteFormItem(Guid id);

        Task<List<FormItemDto>> GetListFormItemAsync(Guid formId);

        Task<FormItemDto> GetFormItemByIdAsync(Guid id);

        #endregion

        #region FormRow Operate

        Task AddFormRow(FormRowCreateDto formRowCreateDto);

        Task UpdateFormRow(FormRowUpdateDto formRowUpdateDto);

        Task DeleteFormRow(Guid id);

        Task<List<FormRowDto>> GetListFormRowAsync(Guid formItemId);

        Task<FormRowDto> GetFormRowByIdAsync(Guid id);

        #endregion

        #region FormCol Operate

        Task AddFormCol(FormColCreateDto formColCreateDto);

        Task UpdateFormCol(FormColUpdateDto formColUpdateDto);

        Task DeleteFormCol(Guid id);

        Task<List<FormColDto>> GetListFormColAsync(Guid formRowId);

        Task<FormColDto> GetFormColByIdAsync(Guid id);

        #endregion
    }
}
