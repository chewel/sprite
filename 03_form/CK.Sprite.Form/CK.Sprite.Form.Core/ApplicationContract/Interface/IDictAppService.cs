﻿using CK.Sprite.Framework;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public interface IDictAppService : IAppService
    {
        Task AddDict(DictDto dictDto);

        Task UpdateDict(DictDto dictDto);

        Task DeleteDict(Guid id);

        Task AddDictItem(DictItemDto dictItemDto);

        Task UpdateDictItem(DictItemDto dictItemDto);

        Task DeleteDictItem(Guid id);
    }
}
