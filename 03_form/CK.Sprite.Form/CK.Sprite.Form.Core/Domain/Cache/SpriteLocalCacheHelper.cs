﻿using AutoMapper;
using CK.Sprite.Framework;
using JetBrains.Annotations;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class SpriteLocalCacheHelper
    {
        public static void AddRelationInfos(List<RelasionInfo> relasionInfos, Guid id, int relationType)
        {
            if (!relasionInfos.Any(r => r.Id == id))
            {
                relasionInfos.Add(new RelasionInfo()
                {
                    Id = id,
                    RelationType = relationType
                });
            }
        }

        public static void MakeWrapInfos(object wrapInfos, List<RelasionInfo> relasionInfos)
        {
            if (wrapInfos != null)
            {
                foreach (var wrapInfo in (wrapInfos as JArray))
                {
                    ChangeStringToObject(wrapInfo, "wrapConfigs");

                    if (wrapInfo["businessCategory"].ToString() == "View")
                    {
                        AddRelationInfos(relasionInfos, Guid.Parse(wrapInfo["objId"].ToString()), 2);
                    }

                    if (wrapInfo["businessCategory"].ToString() == "Form")
                    {
                        AddRelationInfos(relasionInfos, Guid.Parse(wrapInfo["objId"].ToString()), 1);
                    }
                }
            }
        }

        public static void MakeControls(object controls)
        {
            if (controls != null)
            {
                foreach (var control in (controls as JArray))
                {
                    ChangeStringToObject(control, "controlSettings");
                }
            }
        }

        public static JArray MakeRules(string rules)
        {
            // 处理Rule
            JArray addRules = new JArray();
            if (!string.IsNullOrEmpty(rules))
            {
                var dbRules = JsonConvert.DeserializeObject<List<SpriteRule>>(rules, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });


                foreach (var dbRule in dbRules)
                {
                    var addRule = new JObject();
                    var actions = JArray.FromObject(dbRule.Children, new JsonSerializer { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
                    foreach (var action in actions)
                    {
                        ChangeStringToObject(action, "actionConfig");
                    }
                    addRule.Add("actions", actions);

                    dbRule.Children = null;
                    addRule.Add("event", JObject.FromObject(dbRule, new JsonSerializer { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() }));

                    addRules.Add(addRule);
                }
            }
            return addRules;
        }

        public static void ChangeStringToObject(JToken parentToken, string name)
        {
            if (parentToken[name] == null)
            {
                return;
            }
            var strValue = parentToken[name].ToString();
            if (!string.IsNullOrEmpty(strValue))
            {
                parentToken[name] = JToken.Parse(strValue);
            }
        }
    }
}
