﻿using CK.Sprite.Framework;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class ItemViewRowHandler : AbstractSpriteViewHandler, ITransientDependency
    {
        public async Task<long> AddItemViewRow(ItemViewRow itemViewRow, GuidRepositoryBase<ItemViewRow> itemViewRowRepositoryParam = null)
        {
            var itemViewRowRepository = itemViewRowRepositoryParam == null ? new GuidRepositoryBase<ItemViewRow>(DesignUnitOfWork) : itemViewRowRepositoryParam;
            return await itemViewRowRepository.InsertAsync(itemViewRow);
        }

        public async Task<bool> UpdateItemViewRow(ItemViewRow itemViewRow, GuidRepositoryBase<ItemViewRow> itemViewRowRepositoryParam = null)
        {
            var itemViewRowRepository = itemViewRowRepositoryParam == null ? new GuidRepositoryBase<ItemViewRow>(DesignUnitOfWork) : itemViewRowRepositoryParam;
            return await itemViewRowRepository.UpdateAsync(itemViewRow);
        }

        public async Task DeleteItemViewRow(ItemViewRow itemViewRow, GuidRepositoryBase<ItemViewRow> itemViewRowRepositoryParam = null)
        {
            var itemViewRowRepository = itemViewRowRepositoryParam == null ? new GuidRepositoryBase<ItemViewRow>(DesignUnitOfWork) : itemViewRowRepositoryParam;
            await itemViewRowRepository.DeleteAsync(itemViewRow);
        }
    }
}
