﻿using CK.Sprite.Framework;
using System;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class SpriteViewHandler : AbstractSpriteViewHandler, ITransientDependency
    {
        public async Task ChangeListViewFilter(Guid viewId, ISpriteViewRepository spriteViewRepositoryParam = null)
        {
            var spriteViewRepository = spriteViewRepositoryParam == null ? ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(DesignUnitOfWork) : spriteViewRepositoryParam;
            await spriteViewRepository.CommonChangeChildDatas<ListViewFilter>(viewId, "ListViewFilters", "Extension1s", isOrder: true);
        }

        public async Task ChangeListViewCustomerColumn(Guid viewId, ISpriteViewRepository spriteViewRepositoryParam = null)
        {
            var spriteViewRepository = spriteViewRepositoryParam == null ? ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(DesignUnitOfWork) : spriteViewRepositoryParam;
            await spriteViewRepository.CommonChangeChildDatas<ListViewCustomerColumn>(viewId, "ListViewCustomerColumns", "Extension2s");
        }

        public async Task ChangeWrapInfo(Guid viewId, ISpriteViewRepository spriteViewRepositoryParam = null)
        {
            var spriteViewRepository = spriteViewRepositoryParam == null ? ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(DesignUnitOfWork) : spriteViewRepositoryParam;
            await spriteViewRepository.CommonChangeChildDatas<WrapInfo>(viewId, "WrapInfos", "WrapInfos", "BusinessId");
        }

        public async Task ChangeControl(Guid viewId, ISpriteViewRepository spriteViewRepositoryParam = null)
        {
            var spriteViewRepository = spriteViewRepositoryParam == null ? ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(DesignUnitOfWork) : spriteViewRepositoryParam;
            await spriteViewRepository.CommonChangeChildDatas<Control>(viewId, "Controls", "Controls", "BusinessId", isOrder: true);
        }

        public async Task ChangeItemRowOrCol(Guid viewId, ISpriteViewRepository spriteViewRepositoryParam = null)
        {
            var spriteViewRepository = spriteViewRepositoryParam == null ? ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(DesignUnitOfWork) : spriteViewRepositoryParam;
            await spriteViewRepository.ChangeRowColChildDatas(viewId);
        }

        public async Task ChangeRuleOrAction(Guid viewId, ISpriteViewRepository spriteViewRepositoryParam = null)
        {
            var spriteViewRepository = spriteViewRepositoryParam == null ? ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(DesignUnitOfWork) : spriteViewRepositoryParam;
            await spriteViewRepository.ChangeRuleActionChildDatas(viewId);
        }
    }
}
