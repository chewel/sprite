﻿using CK.Sprite.Framework;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class ItemViewColHandler : AbstractSpriteViewHandler, ITransientDependency
    {
        public async Task<long> AddItemViewCol(ItemViewCol itemViewCol, GuidRepositoryBase<ItemViewCol> itemViewColRepositoryParam = null)
        {
            var itemViewColRepository = itemViewColRepositoryParam == null ? new GuidRepositoryBase<ItemViewCol>(DesignUnitOfWork) : itemViewColRepositoryParam;
            return await itemViewColRepository.InsertAsync(itemViewCol);
        }

        public async Task<bool> UpdateItemViewCol(ItemViewCol itemViewCol, GuidRepositoryBase<ItemViewCol> itemViewColRepositoryParam = null)
        {
            var itemViewColRepository = itemViewColRepositoryParam == null ? new GuidRepositoryBase<ItemViewCol>(DesignUnitOfWork) : itemViewColRepositoryParam;
            return await itemViewColRepository.UpdateAsync(itemViewCol);
        }

        public async Task DeleteItemViewCol(ItemViewCol itemViewCol, GuidRepositoryBase<ItemViewCol> itemViewColRepositoryParam = null)
        {
            var itemViewColRepository = itemViewColRepositoryParam == null ? new GuidRepositoryBase<ItemViewCol>(DesignUnitOfWork) : itemViewColRepositoryParam;
            await itemViewColRepository.DeleteAsync(itemViewCol);
        }
    }
}
