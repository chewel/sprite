﻿using CK.Sprite.Framework;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class ListViewFilterHandler : AbstractSpriteViewHandler, ITransientDependency
    {
        public async Task<long> AddListViewFilter(ListViewFilter listViewFilter, GuidRepositoryBase<ListViewFilter> listViewFilterRepositoryParam = null)
        {
            var listViewFilterRepository = listViewFilterRepositoryParam == null ? new GuidRepositoryBase<ListViewFilter>(DesignUnitOfWork) : listViewFilterRepositoryParam;
            return await listViewFilterRepository.InsertAsync(listViewFilter);
        }

        public async Task<bool> UpdateListViewFilter(ListViewFilter listViewFilter, GuidRepositoryBase<ListViewFilter> listViewFilterRepositoryParam = null)
        {
            var listViewFilterRepository = listViewFilterRepositoryParam == null ? new GuidRepositoryBase<ListViewFilter>(DesignUnitOfWork) : listViewFilterRepositoryParam;
            return await listViewFilterRepository.UpdateAsync(listViewFilter);
        }

        public async Task DeleteListViewFilter(ListViewFilter listViewFilter, GuidRepositoryBase<ListViewFilter> listViewFilterRepositoryParam = null)
        {
            var listViewFilterRepository = listViewFilterRepositoryParam == null ? new GuidRepositoryBase<ListViewFilter>(DesignUnitOfWork) : listViewFilterRepositoryParam;
            await listViewFilterRepository.DeleteAsync(listViewFilter);
        }
    }
}
