﻿using CK.Sprite.Cache;
using CK.Sprite.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class SpriteViewService : DomainService
    {
        public ICacheSendNotice CacheSendNotice => LazyGetRequiredService(ref _cacheSendNotice);
        private ICacheSendNotice _cacheSendNotice;

        private string applicationCode;

        #region SpriteView Operate

        public async Task AddSpriteViewAsync(SpriteViewCreateDto spriteViewCreateDto)
        {
            var spriteView = Mapper.Map<SpriteViewCreateDto, SpriteView>(spriteViewCreateDto);
            spriteView.Id = Guid.NewGuid();
            spriteView.Version = Guid.NewGuid();

            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);
                return await spriteViewRepository.InsertAsync(spriteView);
            });
            applicationCode = spriteViewCreateDto.ApplicationCode;
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task UpdateSpriteViewAsync(SpriteViewUpdateDto spriteViewUpdateDto)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);
                var spriteView = await spriteViewRepository.GetAsync(spriteViewUpdateDto.Id);
                if (spriteView == null)
                {
                    throw new SpriteException("未找到SpriteView数据信息");
                }
                applicationCode = spriteView.ApplicationCode;
                Mapper.Map(spriteViewUpdateDto, spriteView);
                spriteView.Version = Guid.NewGuid();
                return await spriteViewRepository.UpdateAsync(spriteView);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task DeleteSpriteView(Guid id)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);
                var deleteData = await spriteViewRepository.GetAsync(id);
                if (deleteData == null)
                {
                    throw new SpriteException("未找到SpriteView数据信息");
                }
                applicationCode = deleteData.ApplicationCode;
                var spriteCommonRepository = ConnectionFactory.GetConnectionProvider(deleteData.ApplicationCode).GetRepository<ISpriteCommonRepository>(unitOfWork);
                await spriteCommonRepository.CheckDeleteViewOrForm(id);
                await spriteViewRepository.DeleteAsync(deleteData);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task<List<SpriteView>> GetSpriteViewAsync(string applicationCode = "Default", int? viewType = null, string filter = default)
        {
            return await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteCommonRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteCommonRepository>(unitOfWork);
                ExpressSqlModel expressSqlModel = new ExpressSqlModel()
                {
                    SqlExpressType = ESqlExpressType.And,
                    Children = new List<ExpressSqlModel>()
                     {
                         new ExpressSqlModel()
                         {
                            Field = "ApplicationCode",
                            ConditionType = EConditionType.等于,
                            Value = applicationCode,
                            SqlExpressType = ESqlExpressType.Condition
                         }
                     }
                };
                if (viewType.HasValue)
                {
                    expressSqlModel.Children.Add(new ExpressSqlModel()
                    {
                        Field = "ViewType",
                        ConditionType = EConditionType.等于,
                        Value = viewType.Value,
                        SqlExpressType = ESqlExpressType.Condition
                    });
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    expressSqlModel.Children.Add(new ExpressSqlModel()
                    {
                        SqlExpressType = ESqlExpressType.Or,
                        Children = new List<ExpressSqlModel>()
                         {
                             new ExpressSqlModel()
                             {
                                Field = "Name",
                                ConditionType = EConditionType.Like,
                                Value = filter,
                                SqlExpressType = ESqlExpressType.Condition
                             },
                             new ExpressSqlModel()
                             {
                                Field = "Description",
                                ConditionType = EConditionType.Like,
                                Value = filter,
                                SqlExpressType = ESqlExpressType.Condition
                             },
                         }
                    });
                }
                var result = await spriteCommonRepository.GetCommonList2<SpriteView>("SpriteViews", expressSqlModel);
                return result;
            });
        }

        #endregion

        #region ListViewFilter Operate

        public async Task AddListViewFilter(ListViewFilterCreateDto listViewFilterCreateDto)
        {
            var listViewFilter = Mapper.Map<ListViewFilterCreateDto, ListViewFilter>(listViewFilterCreateDto);
            listViewFilter.Id = Guid.NewGuid();

            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var listViewFilterRepository = new GuidRepositoryBase<ListViewFilter>(unitOfWork);
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);

                var listViewFilterHandler = _serviceProvider.GetService<ListViewFilterHandler>();
                var spriteViewHandler = _serviceProvider.GetService<SpriteViewHandler>();

                listViewFilterHandler.SetNext(spriteViewHandler, unitOfWork);
                spriteViewHandler.SetNext(null, unitOfWork);

                await listViewFilterHandler.AddListViewFilter(listViewFilter, listViewFilterRepository);
                await spriteViewHandler.ChangeListViewFilter(listViewFilter.ViewId, spriteViewRepository);

                applicationCode = await spriteViewRepository.GetApplicationCodeAsync(CommonConsts.SpriteViewTableName, listViewFilterCreateDto.ViewId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task UpdateListViewFilter(ListViewFilterUpdateDto listViewFilterUpdateDto)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var listViewFilterRepository = new GuidRepositoryBase<ListViewFilter>(unitOfWork);
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);

                var listViewFilter = await listViewFilterRepository.GetAsync(listViewFilterUpdateDto.Id);
                if (listViewFilter == null)
                {
                    throw new SpriteException("未找到ListViewFilter数据信息");
                }
                Mapper.Map(listViewFilterUpdateDto, listViewFilter);

                var listViewFilterHandler = _serviceProvider.GetService<ListViewFilterHandler>();
                var spriteViewHandler = _serviceProvider.GetService<SpriteViewHandler>();

                listViewFilterHandler.SetNext(spriteViewHandler, unitOfWork);
                spriteViewHandler.SetNext(null, unitOfWork);

                await listViewFilterHandler.UpdateListViewFilter(listViewFilter, listViewFilterRepository);
                await spriteViewHandler.ChangeListViewFilter(listViewFilter.ViewId, spriteViewRepository);

                applicationCode = await spriteViewRepository.GetApplicationCodeAsync(CommonConsts.SpriteViewTableName, listViewFilter.ViewId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task DeleteListViewFilter(Guid id)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var listViewFilterRepository = new GuidRepositoryBase<ListViewFilter>(unitOfWork);
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);

                var deleteData = await listViewFilterRepository.GetAsync(id);
                if (deleteData == null)
                {
                    throw new SpriteException("未找到ListViewFilter数据信息");
                }

                var listViewFilterHandler = _serviceProvider.GetService<ListViewFilterHandler>();
                var spriteViewHandler = _serviceProvider.GetService<SpriteViewHandler>();

                listViewFilterHandler.SetNext(spriteViewHandler, unitOfWork);
                spriteViewHandler.SetNext(null, unitOfWork);

                await listViewFilterHandler.DeleteListViewFilter(deleteData, listViewFilterRepository);
                await spriteViewHandler.ChangeListViewFilter(deleteData.ViewId, spriteViewRepository);

                applicationCode = await spriteViewRepository.GetApplicationCodeAsync(CommonConsts.SpriteViewTableName, deleteData.ViewId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task<List<ListViewFilter>> GetListListViewFilterAsync(Guid viewId)
        {
            return await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteCommonRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteCommonRepository>(unitOfWork);
                List<QueryWhereModel> queryWhereModels = new List<QueryWhereModel>() 
                {
                    new QueryWhereModel()
                    {
                        Field = "ViewId",
                        ConditionType = EConditionType.等于,
                        Value = viewId
                    }
                };
                var result = await spriteCommonRepository.GetCommonList<ListViewFilter>("ListViewFilters", queryWhereModels);
                return result;
            });
        }

        #endregion

        #region ListViewCustomerColumn Operate

        public async Task AddListViewCustomerColumn(ListViewCustomerColumnCreateDto listViewCustomerColumnCreateDto)
        {
            var listViewCustomerColumn = Mapper.Map<ListViewCustomerColumnCreateDto, ListViewCustomerColumn>(listViewCustomerColumnCreateDto);
            listViewCustomerColumn.Id = Guid.NewGuid();

            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var listViewCustomerColumnRepository = new GuidRepositoryBase<ListViewCustomerColumn>(unitOfWork);
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);

                var listViewCustomerColumnHandler = _serviceProvider.GetService<ListViewCustomerColumnHandler>();
                var spriteViewHandler = _serviceProvider.GetService<SpriteViewHandler>();

                listViewCustomerColumnHandler.SetNext(spriteViewHandler, unitOfWork);
                spriteViewHandler.SetNext(null, unitOfWork);

                await listViewCustomerColumnHandler.AddListViewCustomerColumn(listViewCustomerColumn, listViewCustomerColumnRepository);
                await spriteViewHandler.ChangeListViewCustomerColumn(listViewCustomerColumn.ViewId, spriteViewRepository);

                applicationCode = await spriteViewRepository.GetApplicationCodeAsync(CommonConsts.SpriteViewTableName, listViewCustomerColumnCreateDto.ViewId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task UpdateListViewCustomerColumn(ListViewCustomerColumnUpdateDto listViewCustomerColumnUpdateDto)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var listViewCustomerColumnRepository = new GuidRepositoryBase<ListViewCustomerColumn>(unitOfWork);
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);

                var listViewCustomerColumn = await listViewCustomerColumnRepository.GetAsync(listViewCustomerColumnUpdateDto.Id);
                if (listViewCustomerColumn == null)
                {
                    throw new SpriteException("未找到ListViewCustomerColumn数据信息");
                }
                Mapper.Map(listViewCustomerColumnUpdateDto, listViewCustomerColumn);

                var listViewCustomerColumnHandler = _serviceProvider.GetService<ListViewCustomerColumnHandler>();
                var spriteViewHandler = _serviceProvider.GetService<SpriteViewHandler>();

                listViewCustomerColumnHandler.SetNext(spriteViewHandler, unitOfWork);
                spriteViewHandler.SetNext(null, unitOfWork);

                await listViewCustomerColumnHandler.UpdateListViewCustomerColumn(listViewCustomerColumn, listViewCustomerColumnRepository);
                await spriteViewHandler.ChangeListViewCustomerColumn(listViewCustomerColumn.ViewId, spriteViewRepository);

                applicationCode = await spriteViewRepository.GetApplicationCodeAsync(CommonConsts.SpriteViewTableName, listViewCustomerColumn.ViewId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task DeleteListViewCustomerColumn(Guid id)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var listViewCustomerColumnRepository = new GuidRepositoryBase<ListViewCustomerColumn>(unitOfWork);
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);

                var deleteData = await listViewCustomerColumnRepository.GetAsync(id);
                if (deleteData == null)
                {
                    throw new SpriteException("未找到ListViewCustomerColumn数据信息");
                }

                var listViewCustomerColumnHandler = _serviceProvider.GetService<ListViewCustomerColumnHandler>();
                var spriteViewHandler = _serviceProvider.GetService<SpriteViewHandler>();

                listViewCustomerColumnHandler.SetNext(spriteViewHandler, unitOfWork);
                spriteViewHandler.SetNext(null, unitOfWork);

                await listViewCustomerColumnHandler.DeleteListViewCustomerColumn(deleteData, listViewCustomerColumnRepository);
                await spriteViewHandler.ChangeListViewCustomerColumn(deleteData.ViewId, spriteViewRepository);

                applicationCode = await spriteViewRepository.GetApplicationCodeAsync(CommonConsts.SpriteViewTableName, deleteData.ViewId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task<List<ListViewCustomerColumn>> GetListListViewCustomerColumnAsync(Guid viewId)
        {
            return await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteCommonRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteCommonRepository>(unitOfWork);
                List<QueryWhereModel> queryWhereModels = new List<QueryWhereModel>()
                {
                    new QueryWhereModel()
                    {
                        Field = "ViewId",
                        ConditionType = EConditionType.等于,
                        Value = viewId
                    }
                };
                var result = await spriteCommonRepository.GetCommonList<ListViewCustomerColumn>("ListViewCustomerColumns", queryWhereModels);
                return result;
            });
        }

        #endregion

        #region ItemViewRow Operate

        public async Task AddItemViewRow(ItemViewRowCreateDto itemViewRowCreateDto)
        {
            var itemViewRow = Mapper.Map<ItemViewRowCreateDto, ItemViewRow>(itemViewRowCreateDto);
            itemViewRow.Id = Guid.NewGuid();

            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var itemViewRowRepository = new GuidRepositoryBase<ItemViewRow>(unitOfWork);
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);

                var itemViewRowHandler = _serviceProvider.GetService<ItemViewRowHandler>();
                var spriteViewHandler = _serviceProvider.GetService<SpriteViewHandler>();

                itemViewRowHandler.SetNext(spriteViewHandler, unitOfWork);
                spriteViewHandler.SetNext(null, unitOfWork);

                await itemViewRowHandler.AddItemViewRow(itemViewRow, itemViewRowRepository);
                await spriteViewHandler.ChangeItemRowOrCol(itemViewRow.ViewId, spriteViewRepository);

                applicationCode = await spriteViewRepository.GetApplicationCodeAsync(CommonConsts.SpriteViewTableName, itemViewRowCreateDto.ViewId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task UpdateItemViewRow(ItemViewRowUpdateDto itemViewRowUpdateDto)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var itemViewRowRepository = new GuidRepositoryBase<ItemViewRow>(unitOfWork);
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);

                var itemViewRow = await itemViewRowRepository.GetAsync(itemViewRowUpdateDto.Id);
                if (itemViewRow == null)
                {
                    throw new SpriteException("未找到ItemViewRow数据信息");
                }
                Mapper.Map(itemViewRowUpdateDto, itemViewRow);

                var itemViewRowHandler = _serviceProvider.GetService<ItemViewRowHandler>();
                var spriteViewHandler = _serviceProvider.GetService<SpriteViewHandler>();

                itemViewRowHandler.SetNext(spriteViewHandler, unitOfWork);
                spriteViewHandler.SetNext(null, unitOfWork);

                await itemViewRowHandler.UpdateItemViewRow(itemViewRow, itemViewRowRepository);
                await spriteViewHandler.ChangeItemRowOrCol(itemViewRow.ViewId, spriteViewRepository);

                applicationCode = await spriteViewRepository.GetApplicationCodeAsync(CommonConsts.SpriteViewTableName, itemViewRow.ViewId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task DeleteItemViewRow(Guid id)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var itemViewRowRepository = new GuidRepositoryBase<ItemViewRow>(unitOfWork);
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);

                var deleteData = await itemViewRowRepository.GetAsync(id);
                if (deleteData == null)
                {
                    throw new SpriteException("未找到ItemViewRow数据信息");
                }

                var itemViewRowHandler = _serviceProvider.GetService<ItemViewRowHandler>();
                var spriteViewHandler = _serviceProvider.GetService<SpriteViewHandler>();

                itemViewRowHandler.SetNext(spriteViewHandler, unitOfWork);
                spriteViewHandler.SetNext(null, unitOfWork);

                await itemViewRowHandler.DeleteItemViewRow(deleteData, itemViewRowRepository);
                await spriteViewHandler.ChangeItemRowOrCol(deleteData.ViewId, spriteViewRepository);

                applicationCode = await spriteViewRepository.GetApplicationCodeAsync(CommonConsts.SpriteViewTableName, deleteData.ViewId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task<List<ItemViewRow>> GetListItemViewRowAsync(Guid viewId)
        {
            return await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteCommonRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteCommonRepository>(unitOfWork);
                List<QueryWhereModel> queryWhereModels = new List<QueryWhereModel>()
                {
                    new QueryWhereModel()
                    {
                        Field = "ViewId",
                        ConditionType = EConditionType.等于,
                        Value = viewId
                    }
                };
                var result = await spriteCommonRepository.GetCommonList<ItemViewRow>("ItemViewRows", queryWhereModels);
                return result;
            });
        }

        #endregion

        #region ItemViewCol Operate

        public async Task AddItemViewCol(ItemViewColCreateDto itemViewColCreateDto)
        {
            var itemViewCol = Mapper.Map<ItemViewColCreateDto, ItemViewCol>(itemViewColCreateDto);
            itemViewCol.Id = Guid.NewGuid();

            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var itemViewColRepository = new GuidRepositoryBase<ItemViewCol>(unitOfWork);
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);

                var itemViewColHandler = _serviceProvider.GetService<ItemViewColHandler>();
                var spriteViewHandler = _serviceProvider.GetService<SpriteViewHandler>();

                itemViewColHandler.SetNext(spriteViewHandler, unitOfWork);
                spriteViewHandler.SetNext(null, unitOfWork);

                await itemViewColHandler.AddItemViewCol(itemViewCol, itemViewColRepository);
                await spriteViewHandler.ChangeItemRowOrCol(itemViewCol.ViewId, spriteViewRepository);

                applicationCode = await spriteViewRepository.GetApplicationCodeAsync(CommonConsts.SpriteViewTableName, itemViewColCreateDto.ViewId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task UpdateItemViewCol(ItemViewColUpdateDto itemViewColUpdateDto)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var itemViewColRepository = new GuidRepositoryBase<ItemViewCol>(unitOfWork);
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);

                var itemViewCol = await itemViewColRepository.GetAsync(itemViewColUpdateDto.Id);
                if (itemViewCol == null)
                {
                    throw new SpriteException("未找到ItemViewCol数据信息");
                }
                Mapper.Map(itemViewColUpdateDto, itemViewCol);

                var itemViewColHandler = _serviceProvider.GetService<ItemViewColHandler>();
                var spriteViewHandler = _serviceProvider.GetService<SpriteViewHandler>();

                itemViewColHandler.SetNext(spriteViewHandler, unitOfWork);
                spriteViewHandler.SetNext(null, unitOfWork);

                await itemViewColHandler.UpdateItemViewCol(itemViewCol, itemViewColRepository);
                await spriteViewHandler.ChangeItemRowOrCol(itemViewCol.ViewId, spriteViewRepository);

                applicationCode = await spriteViewRepository.GetApplicationCodeAsync(CommonConsts.SpriteViewTableName, itemViewCol.ViewId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task DeleteItemViewCol(Guid id)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var itemViewColRepository = new GuidRepositoryBase<ItemViewCol>(unitOfWork);
                var spriteViewRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteViewRepository>(unitOfWork);

                var deleteData = await itemViewColRepository.GetAsync(id);
                if (deleteData == null)
                {
                    throw new SpriteException("未找到ItemViewCol数据信息");
                }

                var itemViewColHandler = _serviceProvider.GetService<ItemViewColHandler>();
                var spriteViewHandler = _serviceProvider.GetService<SpriteViewHandler>();

                itemViewColHandler.SetNext(spriteViewHandler, unitOfWork);
                spriteViewHandler.SetNext(null, unitOfWork);

                await itemViewColHandler.DeleteItemViewCol(deleteData, itemViewColRepository);
                await spriteViewHandler.ChangeItemRowOrCol(deleteData.ViewId, spriteViewRepository);

                applicationCode = await spriteViewRepository.GetApplicationCodeAsync(CommonConsts.SpriteViewTableName, deleteData.ViewId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        public async Task<List<ItemViewCol>> GetListItemViewColAsync(Guid itemViewRowId)
        {
            return await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteCommonRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteCommonRepository>(unitOfWork);
                List<QueryWhereModel> queryWhereModels = new List<QueryWhereModel>()
                {
                    new QueryWhereModel()
                    {
                        Field = "ItemViewRowId",
                        ConditionType = EConditionType.等于,
                        Value = itemViewRowId
                    }
                };
                var result = await spriteCommonRepository.GetCommonList<ItemViewCol>("ItemViewCols", queryWhereModels);
                return result;
            });
        }

        #endregion
    }
}
