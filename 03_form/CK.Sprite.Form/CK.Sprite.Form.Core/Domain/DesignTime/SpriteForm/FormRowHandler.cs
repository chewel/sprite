﻿using CK.Sprite.Framework;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class FormRowHandler : AbstractSpriteViewHandler, ITransientDependency
    {
        public async Task<long> AddFormRow(FormRow formRow, GuidRepositoryBase<FormRow> formRowRepositoryParam = null)
        {
            var formRowRepository = formRowRepositoryParam == null ? new GuidRepositoryBase<FormRow>(DesignUnitOfWork) : formRowRepositoryParam;
            return await formRowRepository.InsertAsync(formRow);
        }

        public async Task<bool> UpdateFormRow(FormRow formRow, GuidRepositoryBase<FormRow> formRowRepositoryParam = null)
        {
            var formRowRepository = formRowRepositoryParam == null ? new GuidRepositoryBase<FormRow>(DesignUnitOfWork) : formRowRepositoryParam;
            return await formRowRepository.UpdateAsync(formRow);
        }

        public async Task DeleteFormRow(FormRow formRow, GuidRepositoryBase<FormRow> formRowRepositoryParam = null)
        {
            var formRowRepository = formRowRepositoryParam == null ? new GuidRepositoryBase<FormRow>(DesignUnitOfWork) : formRowRepositoryParam;
            await formRowRepository.DeleteAsync(formRow);
        }
    }
}
