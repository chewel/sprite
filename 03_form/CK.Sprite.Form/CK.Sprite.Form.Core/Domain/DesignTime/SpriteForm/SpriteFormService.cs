﻿using CK.Sprite.Cache;
using CK.Sprite.Framework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class SpriteFormService : DomainService
    {
        public ICacheSendNotice CacheSendNotice => LazyGetRequiredService(ref _cacheSendNotice);
        private ICacheSendNotice _cacheSendNotice;

        public SpriteFormLocalCache SpriteFormLocalCache => LazyGetRequiredService(ref _spriteFormLocalCache);
        private SpriteFormLocalCache _spriteFormLocalCache;

        public SpriteViewLocalCache SpriteViewLocalCache => LazyGetRequiredService(ref _spriteViewLocalCache);
        private SpriteViewLocalCache _spriteViewLocalCache;

        public SpriteObjectLocalCache SpriteObjectLocalCache => LazyGetRequiredService(ref _spriteObjectLocalCache);
        private SpriteObjectLocalCache _spriteObjectLocalCache;

        private string applicationCode;
        private Dictionary<Guid, Guid> idMapes;

        public SpriteFormService()
        {
            idMapes = new Dictionary<Guid, Guid>();
        }

        #region SpriteForm Operate

        public async Task AddSpriteFormAsync(SpriteFormCreateDto spriteFormCreateDto)
        {
            var spriteForm = Mapper.Map<SpriteFormCreateDto, SpriteForm>(spriteFormCreateDto);
            spriteForm.Id = Guid.NewGuid();
            spriteForm.Version = Guid.NewGuid();

            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteFormRepository = new GuidRepositoryBase<SpriteForm>(unitOfWork);

                applicationCode = spriteFormCreateDto.ApplicationCode;
                return await spriteFormRepository.InsertAsync(spriteForm);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
        }

        public async Task UpdateSpriteFormAsync(SpriteFormUpdateDto spriteFormUpdateDto)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteFormRepository = new GuidRepositoryBase<SpriteForm>(unitOfWork);
                var spriteForm = await spriteFormRepository.GetAsync(spriteFormUpdateDto.Id);
                if (spriteForm == null)
                {
                    throw new SpriteException("未找到SpriteForm数据信息");
                }
                Mapper.Map(spriteFormUpdateDto, spriteForm);
                spriteForm.Version = Guid.NewGuid();
                applicationCode = spriteForm.ApplicationCode;
                return await spriteFormRepository.UpdateAsync(spriteForm);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
        }

        public async Task DeleteSpriteForm(Guid id)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                GuidRepositoryBase<SpriteForm> spriteFormRepository = new GuidRepositoryBase<SpriteForm>(unitOfWork);
                var deleteData = await spriteFormRepository.GetAsync(id);
                if (deleteData == null)
                {
                    throw new SpriteException("未找到SpriteForm数据信息");
                }
                ISpriteCommonRepository spriteCommonRepository = ConnectionFactory.GetConnectionProvider(deleteData.ApplicationCode).GetRepository<ISpriteCommonRepository>(unitOfWork);
                await spriteCommonRepository.CheckDeleteViewOrForm(id);
                await spriteFormRepository.DeleteAsync(deleteData);

                applicationCode = deleteData.ApplicationCode;
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
        }

        public async Task<List<SpriteForm>> GetSpriteFormAsync(bool isTemplate, string applicationCode = "Default", int? formType = null, string filter = default)
        {
            return await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteCommonRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteCommonRepository>(unitOfWork);
                ExpressSqlModel expressSqlModel = new ExpressSqlModel()
                {
                    SqlExpressType = ESqlExpressType.And,
                    Children = new List<ExpressSqlModel>()
                     {
                         new ExpressSqlModel()
                         {
                            Field = "ApplicationCode",
                            ConditionType = EConditionType.等于,
                            Value = applicationCode,
                            SqlExpressType = ESqlExpressType.Condition
                         }
                     }
                };
                expressSqlModel.Children.Add(new ExpressSqlModel()
                {
                    Field = "IsTemplate",
                    ConditionType = EConditionType.等于,
                    Value = isTemplate,
                    SqlExpressType = ESqlExpressType.Condition
                });
                if (formType.HasValue)
                {
                    expressSqlModel.Children.Add(new ExpressSqlModel()
                    {
                        Field = "FormType",
                        ConditionType = EConditionType.等于,
                        Value = formType.Value,
                        SqlExpressType = ESqlExpressType.Condition
                    });
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    expressSqlModel.Children.Add(new ExpressSqlModel()
                    {
                        SqlExpressType = ESqlExpressType.Or,
                        Children = new List<ExpressSqlModel>()
                         {
                             new ExpressSqlModel()
                             {
                                Field = "Name",
                                ConditionType = EConditionType.Like,
                                Value = filter,
                                SqlExpressType = ESqlExpressType.Condition
                             },
                             new ExpressSqlModel()
                             {
                                Field = "Description",
                                ConditionType = EConditionType.Like,
                                Value = filter,
                                SqlExpressType = ESqlExpressType.Condition
                             },
                         }
                    });
                }
                var result = await spriteCommonRepository.GetCommonList2<SpriteForm>("SpriteForms", expressSqlModel);
                return result;
            });
        }

        public async Task SetFormTemplate(Guid id, bool isTemplate)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                GuidRepositoryBase<SpriteForm> spriteFormRepository = new GuidRepositoryBase<SpriteForm>(unitOfWork);
                var findSpriteForm = await spriteFormRepository.GetAsync(id);
                if (findSpriteForm == null)
                {
                    throw new SpriteException("未找到SpriteForm数据信息");
                }

                if (findSpriteForm.IsTemplate == isTemplate)
                {
                    throw new SpriteException($"表单已处于{(isTemplate ? "模版" : "非模板")}状态");
                }

                findSpriteForm.IsTemplate = isTemplate;
                await spriteFormRepository.UpdateAsync(findSpriteForm);

                applicationCode = findSpriteForm.ApplicationCode;
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
        }

        #endregion

        #region FormItem Operate

        public async Task AddFormItem(FormItemCreateDto formItemCreateDto)
        {
            var formItem = Mapper.Map<FormItemCreateDto, FormItem>(formItemCreateDto);
            formItem.Id = Guid.NewGuid();

            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var formItemRepository = new GuidRepositoryBase<FormItem>(unitOfWork);
                var spriteFormRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteFormRepository>(unitOfWork);

                var formItemHandler = _serviceProvider.GetService<FormItemHandler>();
                var spriteFormHandler = _serviceProvider.GetService<SpriteFormHandler>();

                formItemHandler.SetNext(spriteFormHandler, unitOfWork);
                spriteFormHandler.SetNext(null, unitOfWork);

                await formItemHandler.AddFormItem(formItem, formItemRepository);
                await spriteFormHandler.ChangeItemRowOrCol(formItem.FormId, spriteFormRepository);

                applicationCode = await spriteFormRepository.GetApplicationCodeAsync(CommonConsts.SpriteFormTableName, formItemCreateDto.FormId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
        }

        public async Task UpdateFormItem(FormItemUpdateDto formItemUpdateDto)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var formItemRepository = new GuidRepositoryBase<FormItem>(unitOfWork);
                var spriteFormRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteFormRepository>(unitOfWork);

                var formItem = await formItemRepository.GetAsync(formItemUpdateDto.Id);
                if (formItem == null)
                {
                    throw new SpriteException("未找到FormItem数据信息");
                }
                Mapper.Map(formItemUpdateDto, formItem);

                var formItemHandler = _serviceProvider.GetService<FormItemHandler>();
                var spriteFormHandler = _serviceProvider.GetService<SpriteFormHandler>();

                formItemHandler.SetNext(spriteFormHandler, unitOfWork);
                spriteFormHandler.SetNext(null, unitOfWork);

                await formItemHandler.UpdateFormItem(formItem, formItemRepository);
                await spriteFormHandler.ChangeItemRowOrCol(formItem.FormId, spriteFormRepository);

                applicationCode = await spriteFormRepository.GetApplicationCodeAsync(CommonConsts.SpriteFormTableName, formItem.FormId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
        }

        public async Task DeleteFormItem(Guid id)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var formItemRepository = new GuidRepositoryBase<FormItem>(unitOfWork);
                var spriteFormRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteFormRepository>(unitOfWork);

                var deleteData = await formItemRepository.GetAsync(id);
                if (deleteData == null)
                {
                    throw new SpriteException("未找到FormItem数据信息");
                }

                var formItemHandler = _serviceProvider.GetService<FormItemHandler>();
                var spriteFormHandler = _serviceProvider.GetService<SpriteFormHandler>();

                formItemHandler.SetNext(spriteFormHandler, unitOfWork);
                spriteFormHandler.SetNext(null, unitOfWork);

                await formItemHandler.DeleteFormItem(deleteData, formItemRepository);
                await spriteFormHandler.ChangeItemRowOrCol(deleteData.FormId, spriteFormRepository);

                applicationCode = await spriteFormRepository.GetApplicationCodeAsync(CommonConsts.SpriteFormTableName, deleteData.FormId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
        }

        public async Task<List<FormItem>> GetListFormItemAsync(Guid formId)
        {
            return await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteCommonRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteCommonRepository>(unitOfWork);
                List<QueryWhereModel> queryWhereModels = new List<QueryWhereModel>()
                {
                    new QueryWhereModel()
                    {
                        Field = "FormId",
                        ConditionType = EConditionType.等于,
                        Value = formId
                    }
                };
                var result = await spriteCommonRepository.GetCommonList<FormItem>("FormItems", queryWhereModels);
                return result;
            });
        }

        #endregion

        #region FormRow Operate

        public async Task AddFormRow(FormRowCreateDto formRowCreateDto)
        {
            var formRow = Mapper.Map<FormRowCreateDto, FormRow>(formRowCreateDto);
            formRow.Id = Guid.NewGuid();

            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var formRowRepository = new GuidRepositoryBase<FormRow>(unitOfWork);
                var spriteFormRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteFormRepository>(unitOfWork);

                var formRowHandler = _serviceProvider.GetService<FormRowHandler>();
                var spriteFormHandler = _serviceProvider.GetService<SpriteFormHandler>();

                formRowHandler.SetNext(spriteFormHandler, unitOfWork);
                spriteFormHandler.SetNext(null, unitOfWork);

                await formRowHandler.AddFormRow(formRow, formRowRepository);
                await spriteFormHandler.ChangeItemRowOrCol(formRow.FormId, spriteFormRepository);

                applicationCode = await spriteFormRepository.GetApplicationCodeAsync(CommonConsts.SpriteFormTableName, formRowCreateDto.FormId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
        }

        public async Task UpdateFormRow(FormRowUpdateDto formRowUpdateDto)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var formRowRepository = new GuidRepositoryBase<FormRow>(unitOfWork);
                var spriteFormRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteFormRepository>(unitOfWork);

                var formRow = await formRowRepository.GetAsync(formRowUpdateDto.Id);
                if (formRow == null)
                {
                    throw new SpriteException("未找到FormRow数据信息");
                }
                Mapper.Map(formRowUpdateDto, formRow);

                var formRowHandler = _serviceProvider.GetService<FormRowHandler>();
                var spriteFormHandler = _serviceProvider.GetService<SpriteFormHandler>();

                formRowHandler.SetNext(spriteFormHandler, unitOfWork);
                spriteFormHandler.SetNext(null, unitOfWork);

                await formRowHandler.UpdateFormRow(formRow, formRowRepository);
                await spriteFormHandler.ChangeItemRowOrCol(formRow.FormId, spriteFormRepository);

                applicationCode = await spriteFormRepository.GetApplicationCodeAsync(CommonConsts.SpriteFormTableName, formRow.FormId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
        }

        public async Task DeleteFormRow(Guid id)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var formRowRepository = new GuidRepositoryBase<FormRow>(unitOfWork);
                var spriteFormRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteFormRepository>(unitOfWork);

                var deleteData = await formRowRepository.GetAsync(id);
                if (deleteData == null)
                {
                    throw new SpriteException("未找到FormRow数据信息");
                }

                var formRowHandler = _serviceProvider.GetService<FormRowHandler>();
                var spriteFormHandler = _serviceProvider.GetService<SpriteFormHandler>();

                formRowHandler.SetNext(spriteFormHandler, unitOfWork);
                spriteFormHandler.SetNext(null, unitOfWork);

                await formRowHandler.DeleteFormRow(deleteData, formRowRepository);
                await spriteFormHandler.ChangeItemRowOrCol(deleteData.FormId, spriteFormRepository);

                applicationCode = await spriteFormRepository.GetApplicationCodeAsync(CommonConsts.SpriteFormTableName, deleteData.FormId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
        }

        public async Task<List<FormRow>> GetListFormRowAsync(Guid formItemId)
        {
            return await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteCommonRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteCommonRepository>(unitOfWork);
                List<QueryWhereModel> queryWhereModels = new List<QueryWhereModel>()
                {
                    new QueryWhereModel()
                    {
                        Field = "FormItemId",
                        ConditionType = EConditionType.等于,
                        Value = formItemId
                    }
                };
                var result = await spriteCommonRepository.GetCommonList<FormRow>("FormRows", queryWhereModels);
                return result;
            });
        }

        #endregion

        #region FormCol Operate

        public async Task AddFormCol(FormColCreateDto formColCreateDto)
        {
            var formCol = Mapper.Map<FormColCreateDto, FormCol>(formColCreateDto);
            formCol.Id = Guid.NewGuid();

            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var formColRepository = new GuidRepositoryBase<FormCol>(unitOfWork);
                var spriteFormRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteFormRepository>(unitOfWork);

                var formColHandler = _serviceProvider.GetService<FormColHandler>();
                var spriteFormHandler = _serviceProvider.GetService<SpriteFormHandler>();

                formColHandler.SetNext(spriteFormHandler, unitOfWork);
                spriteFormHandler.SetNext(null, unitOfWork);

                await formColHandler.AddFormCol(formCol, formColRepository);
                await spriteFormHandler.ChangeItemRowOrCol(formCol.FormId, spriteFormRepository);

                applicationCode = await spriteFormRepository.GetApplicationCodeAsync(CommonConsts.SpriteFormTableName, formColCreateDto.FormId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
        }

        public async Task UpdateFormCol(FormColUpdateDto formColUpdateDto)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var formColRepository = new GuidRepositoryBase<FormCol>(unitOfWork);
                var spriteFormRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteFormRepository>(unitOfWork);

                var formCol = await formColRepository.GetAsync(formColUpdateDto.Id);
                if (formCol == null)
                {
                    throw new SpriteException("未找到FormCol数据信息");
                }
                Mapper.Map(formColUpdateDto, formCol);

                var formColHandler = _serviceProvider.GetService<FormColHandler>();
                var spriteFormHandler = _serviceProvider.GetService<SpriteFormHandler>();

                formColHandler.SetNext(spriteFormHandler, unitOfWork);
                spriteFormHandler.SetNext(null, unitOfWork);

                await formColHandler.UpdateFormCol(formCol, formColRepository);
                await spriteFormHandler.ChangeItemRowOrCol(formCol.FormId, spriteFormRepository);

                applicationCode = await spriteFormRepository.GetApplicationCodeAsync(CommonConsts.SpriteFormTableName, formCol.FormId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
        }

        public async Task DeleteFormCol(Guid id)
        {
            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var formColRepository = new GuidRepositoryBase<FormCol>(unitOfWork);
                var spriteFormRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteFormRepository>(unitOfWork);

                var deleteData = await formColRepository.GetAsync(id);
                if (deleteData == null)
                {
                    throw new SpriteException("未找到FormCol数据信息");
                }

                var formColHandler = _serviceProvider.GetService<FormColHandler>();
                var spriteFormHandler = _serviceProvider.GetService<SpriteFormHandler>();

                formColHandler.SetNext(spriteFormHandler, unitOfWork);
                spriteFormHandler.SetNext(null, unitOfWork);

                await formColHandler.DeleteFormCol(deleteData, formColRepository);
                await spriteFormHandler.ChangeItemRowOrCol(deleteData.FormId, spriteFormRepository);

                applicationCode = await spriteFormRepository.GetApplicationCodeAsync(CommonConsts.SpriteFormTableName, deleteData.FormId);
            });
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
        }

        public async Task<List<FormCol>> GetListFormColAsync(Guid formRowId)
        {
            return await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteCommonRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteCommonRepository>(unitOfWork);
                List<QueryWhereModel> queryWhereModels = new List<QueryWhereModel>()
                {
                    new QueryWhereModel()
                    {
                        Field = "FormRowId",
                        ConditionType = EConditionType.等于,
                        Value = formRowId
                    }
                };
                var result = await spriteCommonRepository.GetCommonList<FormCol>("FormCols", queryWhereModels);
                return result;
            });
        }

        #endregion

        #region Form Template

        public async Task CreateFormFromTemplate(Guid formId, Guid objectId, string formName, string description, string applicationCode, string templateObjectName, int itemRowColCount = 2)
        {
            var findSpriteForm = await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteFormRepository = new GuidRepositoryBase<SpriteForm>(unitOfWork);
                var dbSpriteForm = await spriteFormRepository.GetAsync(formId);
                if (dbSpriteForm == null)
                {
                    throw new SpriteException("模板信息未找到");
                }
                return dbSpriteForm;
            });


            var formViewVueInfos = new FormViewVueInfos();

            var spriteFormCaches = SpriteFormLocalCache.GetAllDict(findSpriteForm.ApplicationCode);
            var spriteViewCaches = SpriteViewLocalCache.GetAllDict(findSpriteForm.ApplicationCode);

            CalculateFormViewVueInfo(formViewVueInfos, spriteFormCaches, spriteViewCaches, formId, true);
            var spriteObject = SpriteObjectLocalCache.GetAll(findSpriteForm.ApplicationCode).FirstOrDefault(r => r.Id == objectId);

            var formIds = formViewVueInfos.FormDatas.Select(r => r.Id).ToList();
            var viewIds = formViewVueInfos.ViewDatas.Select(r => r.Id).ToList();

            List<SpriteForm> spriteForms = new List<SpriteForm>();
            List<SpriteView> spriteViews = new List<SpriteView>();

            List<Control> formControls = new List<Control>();
            List<Control> viewControls = new List<Control>();
            List<SpriteRule> formSpriteRules = new List<SpriteRule>();
            List<SpriteRule> viewSpriteRules = new List<SpriteRule>();
            List<RuleAction> formRuleActions = new List<RuleAction>();
            List<RuleAction> viewRuleActions = new List<RuleAction>();
            List<WrapInfo> formWrapInfos = new List<WrapInfo>();
            List<WrapInfo> viewWrapInfos = new List<WrapInfo>();

            List<FormItem> formItems = new List<FormItem>();
            List<FormRow> formRows = new List<FormRow>();
            List<FormCol> formCols = new List<FormCol>();

            List<ItemViewRow> itemViewRows = new List<ItemViewRow>();
            List<ItemViewCol> itemViewCols = new List<ItemViewCol>();
            List<ListViewFilter> listViewFilters = new List<ListViewFilter>();
            //List<ListViewCustomerColumn> listViewCustomerColumns = new List<ListViewCustomerColumn>();

            await _serviceProvider.DoDapperServiceAsync(DefaultDbConfig, async (unitOfWork) =>
            {
                var spriteCommonRepository = ConnectionFactory.GetConnectionProvider(DefaultDbConfig.ConnectionType).GetRepository<ISpriteCommonRepository>(unitOfWork);
                List<QueryWhereModel> queryBusinessFormWhereModels = new List<QueryWhereModel>()
                {
                    new QueryWhereModel()
                    {
                        Field = "BusinessId",
                        ConditionType = EConditionType.In,
                        Value = formIds
                    }
                };

                List<QueryWhereModel> queryBusinessViewWhereModels = new List<QueryWhereModel>()
                {
                    new QueryWhereModel()
                    {
                        Field = "BusinessId",
                        ConditionType = EConditionType.In,
                        Value = viewIds
                    }
                };

                List<QueryWhereModel> queryFormIdWhereModels = new List<QueryWhereModel>()
                {
                    new QueryWhereModel()
                    {
                        Field = "FormId",
                        ConditionType = EConditionType.In,
                        Value = formIds
                    }
                };

                List<QueryWhereModel> queryViewIdWhereModels = new List<QueryWhereModel>()
                {
                    new QueryWhereModel()
                    {
                        Field = "ViewId",
                        ConditionType = EConditionType.In,
                        Value = viewIds
                    }
                };

                List<QueryWhereModel> queryIdFormWhereModels = new List<QueryWhereModel>()
                {
                    new QueryWhereModel()
                    {
                        Field = "Id",
                        ConditionType = EConditionType.In,
                        Value = formIds
                    }
                };

                List<QueryWhereModel> queryIdViewWhereModels = new List<QueryWhereModel>()
                {
                    new QueryWhereModel()
                    {
                        Field = "Id",
                        ConditionType = EConditionType.In,
                        Value = viewIds
                    }
                };
                spriteForms = await spriteCommonRepository.GetCommonList<SpriteForm>("SpriteForms", queryIdFormWhereModels);
                spriteViews = await spriteCommonRepository.GetCommonList<SpriteView>("SpriteViews", queryIdViewWhereModels);

                formControls = await spriteCommonRepository.GetCommonList<Control>("Controls", queryBusinessFormWhereModels);
                viewControls = await spriteCommonRepository.GetCommonList<Control>("Controls", queryBusinessViewWhereModels);
                formSpriteRules = await spriteCommonRepository.GetCommonList<SpriteRule>("SpriteRules", queryBusinessFormWhereModels);
                viewSpriteRules = await spriteCommonRepository.GetCommonList<SpriteRule>("SpriteRules", queryBusinessViewWhereModels);
                formRuleActions = await spriteCommonRepository.GetCommonList<RuleAction>("RuleActions", queryBusinessFormWhereModels);
                viewRuleActions = await spriteCommonRepository.GetCommonList<RuleAction>("RuleActions", queryBusinessViewWhereModels);
                formWrapInfos = await spriteCommonRepository.GetCommonList<WrapInfo>("WrapInfos", queryBusinessFormWhereModels);
                viewWrapInfos = await spriteCommonRepository.GetCommonList<WrapInfo>("WrapInfos", queryBusinessViewWhereModels);

                formItems = await spriteCommonRepository.GetCommonList<FormItem>("FormItems", queryFormIdWhereModels);
                formRows = await spriteCommonRepository.GetCommonList<FormRow>("FormRows", queryFormIdWhereModels);
                formCols = await spriteCommonRepository.GetCommonList<FormCol>("FormCols", queryFormIdWhereModels);

                itemViewRows = await spriteCommonRepository.GetCommonList<ItemViewRow>("ItemViewRows", queryViewIdWhereModels);
                itemViewCols = await spriteCommonRepository.GetCommonList<ItemViewCol>("ItemViewCols", queryViewIdWhereModels);
                //listViewFilters = await spriteCommonRepository.GetCommonList<ListViewFilter>("ListViewFilters", queryViewIdWhereModels);
                //listViewCustomerColumns = await spriteCommonRepository.GetCommonList<ListViewCustomerColumn>("ListViewCustomerColumns", queryViewIdWhereModels);
            });

            foreach (var spriteForm in spriteForms)
            {
                CalculateId(spriteForm.Id);
                CalculateId(spriteForm.Version);

                spriteForm.Id = idMapes[spriteForm.Id];
                spriteForm.Version = idMapes[spriteForm.Version];
                spriteForm.ApplicationCode = applicationCode;
                spriteForm.Name = $"{formName}_{Guid.NewGuid()}";
                spriteForm.Description = $"{description}_{Guid.NewGuid()}";
                spriteForm.IsTemplate = false;
            }
            foreach (var spriteView in spriteViews)
            {
                CalculateId(spriteView.Id);
                CalculateId(spriteView.Version);

                spriteView.Id = idMapes[spriteView.Id];
                spriteView.Version = idMapes[spriteView.Version];
                spriteView.ApplicationCode = applicationCode;
                spriteView.Name = $"{formName}_视图_{Guid.NewGuid()}";
                spriteView.Description = $"{description}_视图_{Guid.NewGuid()}";
            }

            foreach (var formControl in formControls)
            {
                CalculateId(formControl.Id);
                CalculateId(formControl.BusinessId);

                formControl.Id = idMapes[formControl.Id];
                formControl.BusinessId = idMapes[formControl.BusinessId];
            }
            foreach (var viewControl in viewControls)
            {
                CalculateId(viewControl.Id);
                CalculateId(viewControl.BusinessId);

                viewControl.Id = idMapes[viewControl.Id];
                viewControl.BusinessId = idMapes[viewControl.BusinessId];
            }
            foreach (var formSpriteRule in formSpriteRules)
            {
                CalculateId(formSpriteRule.Id);
                CalculateId(formSpriteRule.BusinessId);
                CalculateId(formSpriteRule.SubFormId);
                CalculateId(formSpriteRule.SubViewId);

                formSpriteRule.Id = idMapes[formSpriteRule.Id];
                formSpriteRule.BusinessId = idMapes[formSpriteRule.BusinessId];
                if (formSpriteRule.SubFormId.HasValue)
                {
                    formSpriteRule.SubFormId = idMapes[formSpriteRule.SubFormId.Value];
                }
                if (formSpriteRule.SubViewId.HasValue)
                {
                    formSpriteRule.SubViewId = idMapes[formSpriteRule.SubViewId.Value];
                }
            }
            foreach (var viewSpriteRule in viewSpriteRules)
            {
                CalculateId(viewSpriteRule.Id);
                CalculateId(viewSpriteRule.BusinessId);
                CalculateId(viewSpriteRule.SubFormId);
                CalculateId(viewSpriteRule.SubViewId);

                viewSpriteRule.Id = idMapes[viewSpriteRule.Id];
                viewSpriteRule.BusinessId = idMapes[viewSpriteRule.BusinessId];
                if (viewSpriteRule.SubFormId.HasValue)
                {
                    viewSpriteRule.SubFormId = idMapes[viewSpriteRule.SubFormId.Value];
                }
                if (viewSpriteRule.SubViewId.HasValue)
                {
                    viewSpriteRule.SubViewId = idMapes[viewSpriteRule.SubViewId.Value];
                }
            }
            foreach (var formRuleAction in formRuleActions)
            {
                CalculateId(formRuleAction.Id);
                CalculateId(formRuleAction.BusinessId);
                CalculateId(formRuleAction.RuleId);
                CalculateId(formRuleAction.SubFormId);
                CalculateId(formRuleAction.SubViewId);

                formRuleAction.Id = idMapes[formRuleAction.Id];
                formRuleAction.BusinessId = idMapes[formRuleAction.BusinessId];
                formRuleAction.RuleId = idMapes[formRuleAction.RuleId];
                if (formRuleAction.SubFormId.HasValue)
                {
                    formRuleAction.SubFormId = idMapes[formRuleAction.SubFormId.Value];
                }
                if (formRuleAction.SubViewId.HasValue)
                {
                    formRuleAction.SubViewId = idMapes[formRuleAction.SubViewId.Value];
                }
            }
            foreach (var viewRuleAction in viewRuleActions)
            {
                CalculateId(viewRuleAction.Id);
                CalculateId(viewRuleAction.BusinessId);
                CalculateId(viewRuleAction.RuleId);
                CalculateId(viewRuleAction.SubFormId);
                CalculateId(viewRuleAction.SubViewId);

                viewRuleAction.Id = idMapes[viewRuleAction.Id];
                viewRuleAction.BusinessId = idMapes[viewRuleAction.BusinessId];
                viewRuleAction.RuleId = idMapes[viewRuleAction.RuleId];
                if (viewRuleAction.SubFormId.HasValue)
                {
                    viewRuleAction.SubFormId = idMapes[viewRuleAction.SubFormId.Value];
                }
                if (viewRuleAction.SubViewId.HasValue)
                {
                    viewRuleAction.SubViewId = idMapes[viewRuleAction.SubViewId.Value];
                }
            }
            foreach (var formWrapInfo in formWrapInfos)
            {
                CalculateId(formWrapInfo.Id);
                CalculateId(formWrapInfo.BusinessId);
                CalculateId(formWrapInfo.ObjId);

                formWrapInfo.Id = idMapes[formWrapInfo.Id];
                formWrapInfo.BusinessId = idMapes[formWrapInfo.BusinessId];
                if (formWrapInfo.ObjId.HasValue)
                {
                    formWrapInfo.ObjId = idMapes[formWrapInfo.ObjId.Value];
                }
            }
            foreach (var viewWrapInfo in viewWrapInfos)
            {
                CalculateId(viewWrapInfo.Id);
                CalculateId(viewWrapInfo.BusinessId);
                CalculateId(viewWrapInfo.ObjId);

                viewWrapInfo.Id = idMapes[viewWrapInfo.Id];
                viewWrapInfo.BusinessId = idMapes[viewWrapInfo.BusinessId];
                if (viewWrapInfo.ObjId.HasValue)
                {
                    viewWrapInfo.ObjId = idMapes[viewWrapInfo.ObjId.Value];
                }
            }

            foreach (var formItem in formItems)
            {
                CalculateId(formItem.Id);
                CalculateId(formItem.FormId);

                formItem.Id = idMapes[formItem.Id];
                formItem.FormId = idMapes[formItem.FormId];
            }
            foreach (var formRow in formRows)
            {
                CalculateId(formRow.Id);
                CalculateId(formRow.FormId);
                CalculateId(formRow.FormItemId);

                formRow.Id = idMapes[formRow.Id];
                formRow.FormId = idMapes[formRow.FormId];
                formRow.FormItemId = idMapes[formRow.FormItemId];
            }
            foreach (var formCol in formCols)
            {
                CalculateId(formCol.Id);
                CalculateId(formCol.FormId);
                CalculateId(formCol.FormRowId);
                CalculateId(formCol.ObjId);

                formCol.Id = idMapes[formCol.Id];
                formCol.FormId = idMapes[formCol.FormId];
                formCol.FormRowId = idMapes[formCol.FormRowId];
                if (formCol.ObjId.HasValue)
                {
                    formCol.ObjId = idMapes[formCol.ObjId.Value];
                }
            }

            foreach (var itemViewRow in itemViewRows)// 重新计算
            {
                CalculateId(itemViewRow.Id);
                CalculateId(itemViewRow.ViewId);

                itemViewRow.Id = idMapes[itemViewRow.Id];
                itemViewRow.ViewId = idMapes[itemViewRow.ViewId];
            }
            foreach (var itemViewCol in itemViewCols) // 重新计算
            {
                CalculateId(itemViewCol.Id);
                CalculateId(itemViewCol.ViewId);
                CalculateId(itemViewCol.ItemViewRowId);
                CalculateId(itemViewCol.ObjId);

                itemViewCol.Id = idMapes[itemViewCol.Id];
                itemViewCol.ViewId = idMapes[itemViewCol.ViewId];
                itemViewCol.ItemViewRowId = idMapes[itemViewCol.ItemViewRowId];
                if (itemViewCol.ObjId.HasValue)
                {
                    itemViewCol.ObjId = idMapes[itemViewCol.ObjId.Value];
                }
            }

            // 替换特定string类型id

            // 重新计算spriteObject相关表单和视图
            var itemViews = spriteViews.Where(r => r.ViewType == EViewType.ItemView).ToList();
            if (itemViews.Count > 0)
            {
                itemViewRows = itemViewRows.Where(r => itemViews.Any(t => t.Id == r.ViewId)).ToList();
                itemViewCols = itemViewCols.Where(r => itemViews.Any(t => t.Id == r.ViewId)).ToList();
                ItemViewRow currentItemViewRow = null;
                foreach (var itemView in itemViews)
                {
                    for (var i = 0; i < spriteObject.ObjectPropertyDtos.Count; i++) // 固定两行
                    {
                        var propertyDto = spriteObject.ObjectPropertyDtos[i];
                        if (i % itemRowColCount == 0)
                        {
                            currentItemViewRow = new ItemViewRow()
                            {
                                Id = Guid.NewGuid(),
                                ViewId = itemView.Id,
                                Children = new List<ItemViewCol>(),
                                Order = (i / itemRowColCount).ToString("00")
                            };
                            itemViewRows.Add(currentItemViewRow);
                        }
                        ItemViewCol itemViewCol = new ItemViewCol()
                        {
                            Id = Guid.NewGuid(),
                            ViewId = itemView.Id,
                            ItemViewRowId = currentItemViewRow.Id,
                            ColType = EColType.Control,
                            LabelValue = propertyDto.Description,
                            Order = (i % itemRowColCount).ToString("00"),
                            FieldIds = propertyDto.Name.ToCamelCase(),
                            PropertySettings = @"{'span':#span#}".Replace("#span#", (24 / itemRowColCount).ToString()),
                            ComponentName = "col-form-common",
                            Description = propertyDto.Description
                        };
                        // todo 详细验证每种类型
                        // todo 日期范围
                        itemViewCol.ValidateRuleJson = propertyDto.IsRequired ? "[{'required':true,'message':'请填写此项数据'}]" : "";
                        switch (propertyDto.FieldType)
                        {
                            case EFieldType.String:
                                itemViewCol.ControlSettings = "{'placeholder':'请输入" + propertyDto.Description + "信息','componentName':'a-input'}";
                                break;
                            case EFieldType.Int:
                                itemViewCol.ControlSettings = "{'placeholder':'请输入" + propertyDto.Description + "信息','componentName':'a-input'}";
                                break;
                            case EFieldType.Bool:
                                itemViewCol.ControlSettings = "{'placeholder':'请输入" + propertyDto.Description + "信息','componentName':'a-switch'}";
                                break;
                            case EFieldType.Date:
                                itemViewCol.ControlSettings = "{'placeholder':'请输入" + propertyDto.Description + "信息','componentName':'a-date-picker'}";
                                break;
                            case EFieldType.DateTime:
                                itemViewCol.ControlSettings = "{'placeholder':'请输入" + propertyDto.Description + "信息','componentName':'a-date-picker'}";
                                break;
                            case EFieldType.Text:
                                itemViewCol.ControlSettings = "{'placeholder':'请输入" + propertyDto.Description + "信息','componentName':'a-textarea'}";
                                break;
                            case EFieldType.Decimal:
                                itemViewCol.ControlSettings = "{'placeholder':'请输入" + propertyDto.Description + "信息','componentName':'a-input'}";
                                break;
                            default:
                                itemViewCol.ControlSettings = "{'placeholder':'请输入" + propertyDto.Description + "信息','componentName':'a-input'}";
                                break;
                        }
                        itemViewCols.Add(itemViewCol);
                        (currentItemViewRow.Children as List<ItemViewCol>).Add(itemViewCol);
                    }
                }
            }

            var listViews = spriteViews.Where(r => r.ViewType == EViewType.ListView).ToList();
            foreach (var listView in listViews)
            {
                Func<ObjectPropertyDto, bool> func = (r) =>
                {
                    return r.FieldType == EFieldType.String || r.FieldType == EFieldType.Date || r.FieldType == EFieldType.DateTime || r.FieldType == EFieldType.Text;
                };
                Func<ObjectPropertyDto, bool> notFunc = (r) =>
                {
                    return !(r.FieldType == EFieldType.String || r.FieldType == EFieldType.Date || r.FieldType == EFieldType.DateTime || r.FieldType == EFieldType.Text);
                };
                var normalProperties = spriteObject.ObjectPropertyDtos.Where(func).ToList();
                for (var i = 0; i < normalProperties.Count; i++)
                {
                    if (i < 5)
                    {
                        TryAddNormalFilter(normalProperties[i], i, listView.Id, listViewFilters);
                    }
                    else
                    {
                        TryAddHightFilter(normalProperties[i], i, listView.Id, listViewFilters);
                    }
                }
                var normal2Properties = spriteObject.ObjectPropertyDtos.Where(notFunc).ToList();
                for (var i = 0; i < normal2Properties.Count; i++)
                {
                    TryAddNormalFilter(normal2Properties[i], i, listView.Id, listViewFilters);
                }
            }

            foreach (var idMape in idMapes)
            {
                var strOldGuid = idMape.Key.ToString();
                var strNewGuid = idMape.Value.ToString();
                foreach (var formCol in formCols)
                {
                    formCol.WrapConfigs = ReplaceGuid(strOldGuid, strNewGuid, formCol.WrapConfigs);
                    formCol.Express = ReplaceGuid(strOldGuid, strNewGuid, formCol.Express);
                    formCol.ControlSettings = ReplaceGuid(strOldGuid, strNewGuid, formCol.ControlSettings);
                    formCol.PropertySettings = ReplaceGuid(strOldGuid, strNewGuid, formCol.PropertySettings);
                }
                foreach(var formRow in formRows)
                {
                    formRow.PropertySettings = ReplaceGuid(strOldGuid, strNewGuid, formRow.PropertySettings);
                }
                foreach (var formItem in formItems)
                {
                    formItem.PropertySettings = ReplaceGuid(strOldGuid, strNewGuid, formItem.PropertySettings);
                }
                foreach (var viewWrapInfo in viewWrapInfos)
                {
                    viewWrapInfo.WrapConfigs = ReplaceGuid(strOldGuid, strNewGuid, viewWrapInfo.WrapConfigs);
                }
                foreach (var formWrapInfo in formWrapInfos)
                {
                    formWrapInfo.WrapConfigs = ReplaceGuid(strOldGuid, strNewGuid, formWrapInfo.WrapConfigs);
                }
                foreach (var viewRuleAction in viewRuleActions)
                {
                    viewRuleAction.ObjId = ReplaceGuid(strOldGuid, strNewGuid, viewRuleAction.ObjId);
                    viewRuleAction.ActionConfig = ReplaceGuid(strOldGuid, strNewGuid, viewRuleAction.ActionConfig);
                }
                foreach (var formRuleAction in formRuleActions)
                {
                    formRuleAction.ObjId = ReplaceGuid(strOldGuid, strNewGuid, formRuleAction.ObjId);
                    formRuleAction.ActionConfig = ReplaceGuid(strOldGuid, strNewGuid, formRuleAction.ActionConfig);
                }
                foreach (var viewSpriteRule in viewSpriteRules)
                {
                    viewSpriteRule.ObjId = ReplaceGuid(strOldGuid, strNewGuid, viewSpriteRule.ObjId);
                }
                foreach (var formSpriteRule in formSpriteRules)
                {
                    formSpriteRule.ObjId = ReplaceGuid(strOldGuid, strNewGuid, formSpriteRule.ObjId);
                }
                foreach (var viewControl in viewControls)
                {
                    viewControl.ControlSettings = ReplaceGuid(strOldGuid, strNewGuid, viewControl.ControlSettings);
                }
                foreach (var formControl in formControls)
                {
                    formControl.ControlSettings = ReplaceGuid(strOldGuid, strNewGuid, formControl.ControlSettings);
                }
                foreach (var spriteForm in spriteForms)
                {
                    spriteForm.PropertySettings = ReplaceGuid(strOldGuid, strNewGuid, spriteForm.PropertySettings);
                    spriteForm.Rules = ReplaceGuid(strOldGuid, strNewGuid, spriteForm.Rules);
                    spriteForm.WrapInfos = ReplaceGuid(strOldGuid, strNewGuid, spriteForm.WrapInfos);
                    spriteForm.Controls = ReplaceGuid(strOldGuid, strNewGuid, spriteForm.Controls);
                    spriteForm.FormItems = ReplaceGuid(strOldGuid, strNewGuid, spriteForm.FormItems);
                    spriteForm.MenuWrapConfigs = ReplaceGuid(strOldGuid, strNewGuid, spriteForm.MenuWrapConfigs);
                }
                foreach (var spriteView in spriteViews)
                {
                    spriteView.PropertySettings = ReplaceGuid(strOldGuid, strNewGuid, spriteView.PropertySettings);
                    spriteView.Rules = ReplaceGuid(strOldGuid, strNewGuid, spriteView.Rules);
                    spriteView.WrapInfos = ReplaceGuid(strOldGuid, strNewGuid, spriteView.WrapInfos);
                    spriteView.Controls = ReplaceGuid(strOldGuid, strNewGuid, spriteView.Controls);
                    switch (spriteView.ViewType)
                    {
                        case EViewType.ItemView:
                            var findItemViewRows = itemViewRows.Where(r => r.ViewId == spriteView.Id).ToList();
                            if (findItemViewRows.Count > 0)
                            {
                                spriteView.Extension1s = JsonConvert.SerializeObject(findItemViewRows, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
                            }
                            break;
                        case EViewType.ListView:
                            var findListViewFilters = listViewFilters.Where(r => r.ViewId == spriteView.Id).ToList();
                            if (findListViewFilters.Count > 0)
                            {
                                spriteView.Extension1s = JsonConvert.SerializeObject(findListViewFilters, new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() });
                            }
                            break;
                        case EViewType.TreeView:
                            break;
                    }
                }
            }

            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteFormCacheKey}");
            CacheSendNotice.SendClearCache($"{applicationCode}_{CommonConsts.SpriteViewCacheKey}");
        }

        private string ReplaceGuid(string strOldGuid, string strNewGuid, string oldValue)
        {
            if(!string.IsNullOrEmpty(oldValue) && oldValue.IndexOf(strOldGuid) > 0)
            {
                return oldValue.Replace(strOldGuid, strNewGuid);
            }
            return oldValue;
        }

        private void TryAddNormalFilter(ObjectPropertyDto objectPropertyDto, int index, Guid viewId, List<ListViewFilter> listViewFilters)
        {
            ListViewFilter listViewFilter = new ListViewFilter()
            {
                Id = Guid.NewGuid(),
                FieldName = objectPropertyDto.Name,
                FilterType = EFilterType.常规查询,
                LabelValue = objectPropertyDto.Description,
                Order = index.ToString("00"),
                ViewId = viewId
            };

            switch (objectPropertyDto.FieldType)
            {
                case EFieldType.String:
                    listViewFilter.ComponentName = "col-form-common";
                    listViewFilter.ControlSettings = "{'placeholder':'请输入" + objectPropertyDto.Description + "信息','componentName':'a-input'}";
                    listViewFilter.DefaultConditionType = EConditionType.Like;
                    listViewFilter.AllowConditionTypes = "5";
                    listViewFilters.Add(listViewFilter);
                    break;
                case EFieldType.Date:
                    listViewFilter.ComponentName = "col-form-common";
                    listViewFilter.ControlSettings = "{'componentName':'a-range-picker','valueFormat':'YYYY-MM-DD'}";
                    listViewFilter.DefaultConditionType = EConditionType.Between;
                    listViewFilter.AllowConditionTypes = "3";
                    listViewFilters.Add(listViewFilter);
                    break;
                case EFieldType.DateTime:
                    listViewFilter.ComponentName = "col-form-common";
                    listViewFilter.ControlSettings = "{'componentName':'a-range-picker','valueFormat':'YYYY-MM-DD'}";
                    listViewFilter.DefaultConditionType = EConditionType.Between;
                    listViewFilter.AllowConditionTypes = "3";
                    listViewFilters.Add(listViewFilter);
                    break;
                case EFieldType.Text:
                    listViewFilter.ComponentName = "col-form-common";
                    listViewFilter.ControlSettings = "{'placeholder':'请输入" + objectPropertyDto.Description + "信息','componentName':'a-input'}";
                    listViewFilter.DefaultConditionType = EConditionType.Like;
                    listViewFilter.AllowConditionTypes = "5";
                    listViewFilters.Add(listViewFilter);
                    break;
            }
        }

        private void TryAddHightFilter(ObjectPropertyDto objectPropertyDto, int index, Guid viewId, List<ListViewFilter> listViewFilters)
        {
            ListViewFilter listViewFilter = new ListViewFilter()
            {
                Id = Guid.NewGuid(),
                FieldName = objectPropertyDto.Name,
                FilterType = EFilterType.高级查询,
                LabelValue = objectPropertyDto.Description,
                Order = index.ToString("00"),
                ViewId = viewId,
                Description = objectPropertyDto.Description,
            };
            switch (objectPropertyDto.FieldType)
            {
                case EFieldType.String:
                    listViewFilter.ComponentName = "TextBox";
                    listViewFilter.ControlSettings = "{'placeholder':'请输入" + objectPropertyDto.Description + "信息'}";
                    listViewFilter.DefaultConditionType = EConditionType.Like;
                    listViewFilter.AllowConditionTypes = "5;1";
                    listViewFilters.Add(listViewFilter);
                    break;
                case EFieldType.Int:
                    listViewFilter.ComponentName = "TextBox";
                    listViewFilter.ControlSettings = "{'placeholder':'请输入" + objectPropertyDto.Description + "信息'}";
                    listViewFilter.DefaultConditionType = EConditionType.等于;
                    listViewFilter.AllowConditionTypes = "1;2;3;6;7;8;9";
                    listViewFilters.Add(listViewFilter);
                    break;
                case EFieldType.Bool:
                    listViewFilter.ComponentName = "Radio";
                    listViewFilter.ControlSettings = "{'placeholder':'请输入" + objectPropertyDto.Description + "信息'}";
                    listViewFilter.DefaultConditionType = EConditionType.等于;
                    listViewFilter.AllowConditionTypes = "1";
                    listViewFilters.Add(listViewFilter);
                    break;
                case EFieldType.Date:
                    listViewFilter.ComponentName = "DateTime";
                    listViewFilter.ControlSettings = "{'placeholder':'请输入" + objectPropertyDto.Description + "信息'}";
                    listViewFilter.DefaultConditionType = EConditionType.Between;
                    listViewFilter.AllowConditionTypes = "3;6;7;8;9";
                    listViewFilters.Add(listViewFilter);
                    break;
                case EFieldType.DateTime:
                    listViewFilter.ComponentName = "DateTime";
                    listViewFilter.ControlSettings = "{'placeholder':'请输入" + objectPropertyDto.Description + "信息'}";
                    listViewFilter.DefaultConditionType = EConditionType.Between;
                    listViewFilter.AllowConditionTypes = "3;6;7;8;9";
                    listViewFilters.Add(listViewFilter);
                    break;
                case EFieldType.Text:
                    listViewFilter.ComponentName = "TextBox";
                    listViewFilter.ControlSettings = "{'placeholder':'请输入" + objectPropertyDto.Description + "信息'}";
                    listViewFilter.DefaultConditionType = EConditionType.Like;
                    listViewFilter.AllowConditionTypes = "5;1";
                    listViewFilters.Add(listViewFilter);
                    break;
                case EFieldType.Float:
                    listViewFilter.ComponentName = "TextBox";
                    listViewFilter.ControlSettings = "{'placeholder':'请输入" + objectPropertyDto.Description + "信息'}";
                    listViewFilter.DefaultConditionType = EConditionType.等于;
                    listViewFilter.AllowConditionTypes = "1;2;3;6;7;8;9";
                    listViewFilters.Add(listViewFilter);
                    break;
                case EFieldType.Decimal:
                    listViewFilter.ComponentName = "TextBox";
                    listViewFilter.ControlSettings = "{'placeholder':'请输入" + objectPropertyDto.Description + "信息'}";
                    listViewFilter.DefaultConditionType = EConditionType.等于;
                    listViewFilter.AllowConditionTypes = "1;2;3;6;7;8;9";
                    listViewFilters.Add(listViewFilter);
                    break;
            }
        }

        private void CalculateId(Guid? oldId)
        {
            if (!oldId.HasValue)
            {
                return;
            }
            if (!idMapes.ContainsKey(oldId.Value))
            {
                if (oldId.Value == Guid.Empty)
                {
                    idMapes.Add(oldId.Value, Guid.Empty);
                }
                else
                {
                    idMapes.Add(oldId.Value, Guid.NewGuid());
                }
            }
        }

        private void CalculateFormViewVueInfo(FormViewVueInfos formViewVueInfos, Dictionary<Guid, SpriteFormVueDto> spriteFormVueDtos, Dictionary<Guid, SpriteViewVueDto> spriteViewVueDtos, Guid id, bool isForm)
        {
            if (isForm)
            {
                spriteFormVueDtos.TryGetValue(id, out var spriteFormVueDto);
                if (spriteFormVueDtos.ContainsKey(id) && !formViewVueInfos.FormDatas.Exists(r => r.Id == spriteFormVueDto.Id))
                {
                    formViewVueInfos.FormDatas.Add(spriteFormVueDto);
                    foreach (var relationInfo in (spriteFormVueDto.RelationInfos as List<RelasionInfo>))
                    {
                        CalculateFormViewVueInfo(formViewVueInfos, spriteFormVueDtos, spriteViewVueDtos, relationInfo.Id, relationInfo.RelationType == 1);
                    }
                }
            }
            else
            {
                spriteViewVueDtos.TryGetValue(id, out var spriteViewVueDto);
                if (spriteViewVueDto != null && !formViewVueInfos.FormDatas.Exists(r => r.Id == spriteViewVueDto.Id))
                {
                    formViewVueInfos.ViewDatas.Add(spriteViewVueDto);
                    foreach (var relationInfo in (spriteViewVueDto.RelationInfos as List<RelasionInfo>))
                    {
                        CalculateFormViewVueInfo(formViewVueInfos, spriteFormVueDtos, spriteViewVueDtos, relationInfo.Id, relationInfo.RelationType == 1);
                    }
                }
            }
        }

        #endregion
    }
}
