﻿using CK.Sprite.Framework;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class FormItemHandler : AbstractSpriteViewHandler, ITransientDependency
    {
        public async Task<long> AddFormItem(FormItem formItem, GuidRepositoryBase<FormItem> formItemRepositoryParam = null)
        {
            var formItemRepository = formItemRepositoryParam == null ? new GuidRepositoryBase<FormItem>(DesignUnitOfWork) : formItemRepositoryParam;
            return await formItemRepository.InsertAsync(formItem);
        }

        public async Task<bool> UpdateFormItem(FormItem formItem, GuidRepositoryBase<FormItem> formItemRepositoryParam = null)
        {
            var formItemRepository = formItemRepositoryParam == null ? new GuidRepositoryBase<FormItem>(DesignUnitOfWork) : formItemRepositoryParam;
            return await formItemRepository.UpdateAsync(formItem);
        }

        public async Task DeleteFormItem(FormItem formItem, GuidRepositoryBase<FormItem> formItemRepositoryParam = null)
        {
            var formItemRepository = formItemRepositoryParam == null ? new GuidRepositoryBase<FormItem>(DesignUnitOfWork) : formItemRepositoryParam;
            await formItemRepository.DeleteAsync(formItem);
        }
    }
}
