﻿using CK.Sprite.Framework;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    public class FormColHandler : AbstractSpriteViewHandler, ITransientDependency
    {
        public async Task<long> AddFormCol(FormCol formCol, GuidRepositoryBase<FormCol> formColRepositoryParam = null)
        {
            var formColRepository = formColRepositoryParam == null ? new GuidRepositoryBase<FormCol>(DesignUnitOfWork) : formColRepositoryParam;
            return await formColRepository.InsertAsync(formCol);
        }

        public async Task<bool> UpdateFormCol(FormCol formCol, GuidRepositoryBase<FormCol> formColRepositoryParam = null)
        {
            var formColRepository = formColRepositoryParam == null ? new GuidRepositoryBase<FormCol>(DesignUnitOfWork) : formColRepositoryParam;
            return await formColRepository.UpdateAsync(formCol);
        }

        public async Task DeleteFormCol(FormCol formCol, GuidRepositoryBase<FormCol> formColRepositoryParam = null)
        {
            var formColRepository = formColRepositoryParam == null ? new GuidRepositoryBase<FormCol>(DesignUnitOfWork) : formColRepositoryParam;
            await formColRepository.DeleteAsync(formCol);
        }
    }
}
