﻿using AutoMapper;
using CK.Sprite.Framework;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CK.Sprite.Form.Core
{
    /// <summary>
    /// 操作职责类定义
    /// </summary>
    public abstract class AbstractSpriteFormHandler : AbstractSpriteHandler
    {
        
    }
}
