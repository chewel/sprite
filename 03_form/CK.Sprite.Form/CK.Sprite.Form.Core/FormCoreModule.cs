﻿using AutoMapper;
using CK.Sprite.Form.ThirdService;
using CK.Sprite.Framework;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CK.Sprite.Form.Core
{
    public class FormCoreModule : SpriteModule
    {
        public override void PreConfigureServices(IServiceCollection Services)
        {
            Services.AddAssemblyOf<FormCoreModule>();

            Services.AddTransient<IFormThirdServiceAppService, RuntimeAppService>();
            Services.AddAutoMapper(typeof(AutomapperConfig));

        }
    }
}
